import 'dart:async';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:study/travel_list/V2/src/item/item-model.dart';
import 'dart:io' as io;
import 'package:study/travel_list/V2/src/viagem/viagem-model.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "main.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate, onUpgrade: _onUpgrade, onDowngrade: _onDowngrade);
    return theDb;
  }

  void _onCreate(Database db, int version) async{
    await db.execute(
        "CREATE TABLE Viagem( id INTEGER PRIMARY KEY AUTOINCREMENT"
            ", nome TEXT )");

    await db.execute(
        "CREATE TABLE Item( id INTEGER PRIMARY KEY AUTOINCREMENT"
            ", viagemId INTEGER"
            ", nome TEXT"
            ", par NUMERIC"
            ", quantidade INTEGER"
            ", checado NUMERIC )");
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) async{
    db.execute("DROP TABLE IF EXISTS Viagem");
    db.execute("DROP TABLE IF EXISTS Item");
    _onCreate(db, newVersion);
  }

  void _onDowngrade(Database db, int oldVersion, int newVersion) async{
    db.execute("DROP TABLE IF EXISTS Viagem");
    db.execute("DROP TABLE IF EXISTS Item");
    _onCreate(db, newVersion);
  }

  Future closeDb() async {
    var dbClient = await db;
    dbClient.close();
  }

  //Item>>>>>>>>
  Future<List<Item>> getItens(int idViagem) async {
    List<Item> itens = new List<Item>();
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Item", where: "viagemId = ?", whereArgs: [idViagem]);
    if (maps.length > 0) {
      for (Map item in maps) {
        itens.add(Item.fromMap(item));
      }
    }
    return itens;
  }

  Future<Item> insertItem(Item item) async {
    var dbClient = await db;
    item.id = await dbClient.insert("Item", item.toMap());
    return item;
  }

  Future<int> deleteItem(int id) async {
    var dbClient = await db;
    return await dbClient.delete("Item", where: "id = ?", whereArgs: [id]);
  }

  Future<int> deleteItemDeViagem(int viagemId) async {
    var dbClient = await db;
    return await dbClient.delete("Item", where: "viagemId = ?", whereArgs: [viagemId]);
  }

  Future<int> updateItem(Item item) async {
    var dbClient = await db;
    int qtd = await dbClient.update("Item", item.toMap(),
        where: "id = ?", whereArgs: [item.id]);
    return qtd;
  }

  //<<<<<<<Item

  //Viagem>>>>>>>>
  Future<Viagem> insertViagem(Viagem viagem) async {
    var dbClient = await db;
    viagem.id = await dbClient.insert("Viagem", viagem.toMap());
    return viagem;
  }

  Future<int> deleteViagem(int id) async {
    var dbClient = await db;
    await deleteItemDeViagem(id);
    return await dbClient.delete("Viagem", where: "id = ?", whereArgs: [id]);
  }

  Future<int> updateViagem(Viagem viagem) async {
    var dbClient = await db;
    int qtd = await dbClient.update("Viagem", viagem.toMap());
    return qtd;
  }

  Future<Viagem> getViagem() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Viagem",
        columns: ["id", "nome"]);
    if (maps.length > 0) {
      return new Viagem.fromMap(maps.first);
    }
    return null;
  }

  Future<List<Viagem>> getViagens() async {
    List<Viagem> viagens = new List<Viagem>();
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Viagem");
    if (maps.length > 0) {
      for (Map viagem in maps) {
        viagens.add(Viagem.fromMap(viagem));
      }
    }
    return viagens;
  }
//<<<<<<<Viagem

}