import 'package:flutter/material.dart';
import 'package:study/travel_list/V2/src/viagem/viagem-edit-widget.dart';
import 'package:study/travel_list/V2/src/viagem/viagem-widget.dart';

final routes = {
  '/viagem': (BuildContext context) => ViagemPage(),
  '/edit_viagem': (BuildContext context) => EditViagemPage(),
};