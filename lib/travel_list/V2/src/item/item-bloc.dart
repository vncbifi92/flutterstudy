import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:study/travel_list/V2/src/item/item-model.dart';
import 'package:study/travel_list/V2/src/shared/service/database.dart';

class ItemBloc extends BlocBase{

  DatabaseHelper databaseHelper = DatabaseHelper.internal();

  var _nome = BehaviorSubject<String>.seeded("");

  Stream<String> get outNome => _nome.stream;

  Sink<String> get inNome => _nome.sink;

  String get nomeValue => _nome.value;

  void salvaNome(String nome){
    inNome.add(nome);
    print("nome");
  }

  var _par = BehaviorSubject<bool>.seeded(false);

  Stream<bool> get outPar => _par.stream;

  Sink<bool> get inPar => _par.sink;

  bool get parValue => _par.value;

  void salvaPar(bool par){
    inPar.add(par);
    print("par");
  }

  var _checado = BehaviorSubject<bool>.seeded(false);

  Stream<bool> get outChecado => _checado.stream;

  Sink<bool> get inChecado => _checado.sink;

  bool get checadoValue => _checado.value;

  void salvaChecado(bool checado){
    inChecado.add(checado);
    print("checado");
  }

  var _quantidade = BehaviorSubject<int>.seeded(0);

  Stream<int> get outQuantidade => _quantidade.stream;

  Sink<int> get inQuantidade => _quantidade.sink;

  int get quantidadeValue => _quantidade.value;

  void salvaQuantidade(int quantidade){
    inQuantidade.add(quantidade);
    print("quantidade");
  }

  var _viagemId = BehaviorSubject<int>.seeded(0);

  Stream<int> get outViagemId => _viagemId.stream;

  Sink<int> get inViagemId  => _viagemId.sink;

  int get viagemIdValue => _viagemId.value;

  void salvaViagemId(int viagemId){
    inViagemId.add(viagemId);
    print("viagemId");
  }

  var _item = BehaviorSubject<Item>.seeded(Item("", false, 0, 0));

  Stream<Item> get outItem => _item.stream;

  Sink<Item> get inItem => _item.sink;

  Item get itemValue => _item.value;

  void salvaItem(Item item){
    inItem.add(item);
    print("item");
  }

  var _listItem = BehaviorSubject<List<Item>>.seeded(List<Item>());

  Stream<List<Item>> get outList => _listItem.stream;

  Sink<List<Item>> get inList => _listItem.sink;

  List<Item> get listItemValue => _listItem.value;

  @override
  void dispose() {
    _viagemId.close();
    _quantidade.close();
    _par.close();
    _nome.close();
    _item.close();
    _checado.close();
    _listItem.close();
  }

  void carregaLista(int idViagem) async{
    List<Item> itens = await databaseHelper.getItens(idViagem);
    itens.sort((a, b) {
      return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
    });
    _listItem.value = itens;
  }

  void add(){
    Item value = Item(_nome.value, _par.value, _quantidade.value, _viagemId.value);
    value.checado = _checado.value;
    databaseHelper.insertItem(value);
    _listItem.value.add(value);
    _listItem.value.sort((a, b) {
      return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
    });
    inList.add(_listItem.value);
    print("salva lista");
  }

  void edit(){
    Item value = _item.value;
    value.nome = _nome.value;
    value.quantidade = _quantidade.value;
    value.par = _par.value;
    value.viagemId = _viagemId.value;
    value.checado =_checado.value;
    value.id = _item.value.id;
    databaseHelper.updateItem(value);
    _listItem.value.sort((a, b) {
      return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
    });
    inList.add(_listItem.value);
  }

  void remove(){
    databaseHelper.deleteItem(_item.value.id);
    _listItem.value.remove(_item.value);
    inList.add(_listItem.value);
    print("remove lista");
  }
}

ItemBloc itemBloc = ItemBloc();