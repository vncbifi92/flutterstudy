class Item{

  int _id;
  int _viagemId;
  String _nome;
  bool _par;
  int _quantidade;
  bool _checado;

  Item(this._nome, this._par, this._quantidade, this._viagemId);

  int get viagemId => _viagemId;

  set viagemId(int value){
    _viagemId = value;
  }
  bool get checado => _checado;

  set checado(bool value){
    _checado = value;
  }

  String get nome => _nome;

  set nome(String value) {
    _nome = value;
  }

  bool get par => _par;

  set par(bool value) {
    _par = value;
  }

  int get quantidade => _quantidade;

  set quantidade(int value) {
    _quantidade = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  @override
  String toString() {
    return 'Item{_id: $_id, _viagemId: $_viagemId, _nome: $_nome, _par: $_par, _quantidade: $_quantidade, _checado: $_checado}';
  }

  @override
  bool operator ==(other) {
    if (_id == other._id && _viagemId == other._viagemId && _nome == other._nome && _par == other._par && _quantidade == other._quantidade) {
      return true;
    }else{
      return false;
    }
  }

  @override
  int get hashCode => super.hashCode;

  Item.fromMap(Map<String, dynamic> map) {
    _id = map['id'];
    _viagemId = map['viagemId'];
    _nome = map['nome'];
    _par = map['par'] == 1 ? true : false;
    _quantidade = map['quantidade'];
    _checado = map['_checado'] == 1 ? true : false;
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'viagemId': this._viagemId,
      'nome': this._nome,
      'par': this._par == true ? 1 : 0,
      'quantidade': this._quantidade,
      'checado': this._checado == true ? 1 : 0
    };
    if (_id != null) {
      map["id"] = _id;
    }
    return map;
  }

}