import 'package:flutter/material.dart';
import 'package:study/travel_list/V2/src/item/item-bloc.dart';
import 'package:study/travel_list/V2/src/item/item-model.dart';

class Dialogs{

  cadastroItem(BuildContext context, int id) {

    final formKey = GlobalKey<FormState>();

    _swipe(ItemBloc bloc){
      if(bloc.parValue){
        bloc.salvaPar(false);
      }else{
        bloc.salvaPar(true);
      }
    }

    _salvaItem(ItemBloc bloc) {
      bloc.inViagemId.add(id);
      final form = formKey.currentState;
      if (form.validate()) {
        form.save();
        bloc.add();
        Navigator.of(context).pop();
      }
    }

    Widget _okButton(ItemBloc bloc){
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Confirmar",
          style: TextStyle(
              color: Colors.orange
          ),
        ),
        onPressed: (){_salvaItem(bloc);},
      );
    }

    Widget _cancelButton(){
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Cancelar",
          style: TextStyle(
              color: Colors.orange
          ),
        ),
        onPressed: (){Navigator.pop(context);},
      );
    }

    Widget _itemForm(ItemBloc bloc){
      bloc.inQuantidade.add(0);
      bloc.inNome.add("");
      bloc.inPar.add(false);
      return Form(
          key: formKey,
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextFormField(
                  onSaved: (String val){bloc.salvaNome(val);},
                  decoration: InputDecoration(
                      labelText: "Nome",
                      border: OutlineInputBorder()),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: OutlineButton(
                          onPressed: (){bloc.salvaQuantidade(bloc.quantidadeValue-1);},
                          child: Icon(Icons.remove),
                          shape: CircleBorder()
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: StreamBuilder(
                            stream: bloc.outQuantidade,
                            builder: (BuildContext context, AsyncSnapshot snapshot) {
                              return TextFormField(
                                enabled: false,
                                controller: TextEditingController(text: snapshot.data.toString()),
                                onSaved: (String val){bloc.salvaQuantidade(int.parse(val));},
                                decoration: InputDecoration(
                                    labelText: "Quantidade",
                                    border: UnderlineInputBorder()),
                              );
                            }
                        ),
                      ),
                    ),
                    Expanded(
                      child: OutlineButton(
                          onPressed: (){bloc.salvaQuantidade(bloc.quantidadeValue+1);},
                          child: Icon(Icons.add),
                          shape: CircleBorder()
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(child: Text("É par?")),
                    StreamBuilder(
                      stream: bloc.outPar,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        return IconButton(
                            icon: Icon(snapshot.data == true ? Icons.people : Icons.people_outline),
                            onPressed: (){
                              _swipe(itemBloc);
                            }
                        );
                      },
                    ),
                  ],
                ),
              ],
            ),
          )
      );
    }

    return  showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(
            content: _itemForm(itemBloc),
            actions: <Widget>[
              _cancelButton(),
              _okButton(itemBloc)
            ],
          );
        }
    );
  }

  editarItem(BuildContext context, Item item) {

    final formKey = GlobalKey<FormState>();

    _swipe(ItemBloc bloc){
      if(bloc.parValue){
        bloc.salvaPar(false);
      }else{
        bloc.salvaPar(true);
      }
    }

    _editaItem(ItemBloc bloc) {
      final form = formKey.currentState;
      if (form.validate()) {
        form.save();
        bloc.edit();
        Navigator.of(context).pop();
      }
    }

    Widget _okButton(ItemBloc bloc){
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Confirmar",
          style: TextStyle(
              color: Colors.orange
          ),
        ),
        onPressed: (){_editaItem(bloc);},
      );
    }

    Widget _cancelButton(){
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Cancelar",
          style: TextStyle(
              color: Colors.orange
          ),
        ),
        onPressed: (){Navigator.pop(context);},
      );
    }

    Widget _itemForm(ItemBloc bloc){
      bloc.inQuantidade.add(item.quantidade);
      bloc.inNome.add(item.nome);
      bloc.inPar.add(item.par);
      bloc.inViagemId.add(item.viagemId);
      bloc.inItem.add(item);
      return Form(
          key: formKey,
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextFormField(
                  initialValue: bloc.nomeValue,
                  onSaved: (String val){bloc.salvaNome(val);},
                  decoration: InputDecoration(
                      labelText: "Nome",
                      border: OutlineInputBorder()),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: OutlineButton(
                          onPressed: (){bloc.salvaQuantidade(bloc.quantidadeValue-1);},
                          child: Icon(Icons.remove),
                          shape: CircleBorder()
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: StreamBuilder(
                            stream: bloc.outQuantidade,
                            builder: (BuildContext context, AsyncSnapshot snapshot) {
                              return TextFormField(
                                enabled: false,
                                controller: TextEditingController(text: snapshot.data.toString()),
                                onSaved: (String val){bloc.salvaQuantidade(int.parse(val));},
                                decoration: InputDecoration(
                                    labelText: "Quantidade",
                                    border: UnderlineInputBorder()),
                              );
                            }
                        ),
                      ),
                    ),
                    Expanded(
                      child: OutlineButton(
                          onPressed: (){bloc.salvaQuantidade(bloc.quantidadeValue+1);},
                          child: Icon(Icons.add),
                          shape: CircleBorder()
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(child: Text("É par?")),
                    StreamBuilder(
                      stream: bloc.outPar,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        return IconButton(
                            icon: Icon(snapshot.data == true ? Icons.people : Icons.people_outline),
                            onPressed: (){
                              _swipe(itemBloc);
                            }
                        );
                      },
                    ),
                  ],
                ),
              ],
            ),
          )
      );
    }

    return  showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(
            content: _itemForm(itemBloc),
            actions: <Widget>[
              _cancelButton(),
              _okButton(itemBloc)
            ],
          );
        }
    );
  }


}