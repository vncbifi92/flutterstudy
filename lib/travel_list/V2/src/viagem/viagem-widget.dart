import 'package:flutter/material.dart';
import 'package:study/travel_list/V2/src/viagem/viagem-bloc.dart';
import 'package:study/travel_list/V2/src/viagem/viagem-dialogs.dart';
import 'package:study/travel_list/V2/src/viagem/viagem-model.dart';

class ViagemPage extends StatefulWidget {
  ViagemPage({Key key}) : super(key: key);


  @override
  _ViagemPageState createState() => new _ViagemPageState();
}


class _ViagemPageState extends State<ViagemPage>{


  _removeViagem(ViagemBloc bloc, Viagem viagem) {
    bloc.salvaViagem(viagem);
    bloc.remove();
  }


  _editarViagem(ViagemBloc bloc, Viagem viagem) {
    bloc.salvaNome(viagem.nome);
    bloc.salvaViagem(viagem);
    Navigator.of(context).pushNamed('/edit_viagem');
  }


  _newTravel(){
    Dialogs dialogs = new Dialogs();
    dialogs.cadastroViagem(context);
  }


  Widget _listaViagens(ViagemBloc bloc){
    return StreamBuilder(
        stream: bloc.outList,
        builder: (BuildContext context, AsyncSnapshot<List<Viagem>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                itemBuilder: (context, index) {
                  if (index < snapshot.data.length) {
                    return Column(
                      children: <Widget>[
                        GestureDetector(
                          child: ListTile(
                            title: Text(
                                snapshot.data.elementAt(index).nome
                            ),
                            onTap: () => _editarViagem(bloc, snapshot.data.elementAt(index)),
                          ),
                          onLongPress: () =>
                              _removeViagem(bloc, snapshot.data.elementAt(index)),
                        ),
                        Divider(),
                      ],
                    );
                  }
                }
            );
          }else{
            return Divider();
          }
        }
    );
  }


  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            "Viagens",
            style: TextStyle(
                color: Colors.black87
            ),
          )
      ),
      body: Container(
          padding: EdgeInsets.all(10.0),
          child: _listaViagens(viagemBloc)
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 60,
          color: Colors.white,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.deepOrange,
          icon: Icon(Icons.add),
          label: Text("Nova Viagem"),
          onPressed: _newTravel
      ),
    );
  }
}