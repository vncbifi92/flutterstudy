class Viagem{

  int _id;
  String _nome;

  Viagem(this._nome);

  String get nome => _nome;

  set nome(String value) {
    _nome = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  @override
  String toString() {
    return 'Viagem{_id: $_id, _nome: $_nome}';
  }

  @override
  bool operator ==(other) {
    if (_id == other._id && _nome == other._nome) {
      return true;
    }else{
      return false;
    }
  }

  @override
  int get hashCode => super.hashCode;

  Viagem.fromMap(Map<String, dynamic> map) {
    _id = map['id'];
    _nome = map['nome'];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'nome': this._nome,
    };
    if (_id != null) {
      map["id"] = _id;
    }
    return map;
  }

}