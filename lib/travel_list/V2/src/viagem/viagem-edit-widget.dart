import 'package:flutter/material.dart';
import 'package:study/travel_list/V2/src/item/item-bloc.dart';
import 'package:study/travel_list/V2/src/item/item-dialogs.dart';
import 'package:study/travel_list/V2/src/item/item-model.dart';
import 'package:study/travel_list/V2/src/viagem/viagem-bloc.dart';

class EditViagemPage extends StatefulWidget {
  EditViagemPage({Key key}) : super(key: key);

  @override
  _EditViagemPageState createState() => new _EditViagemPageState();
}

class _EditViagemPageState extends State<EditViagemPage>{
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  _cadastroItem(int id){
    Dialogs dialogs = new Dialogs();
    dialogs.cadastroItem(context, id);
  }

  _removeItem(ItemBloc bloc, Item item){
    bloc.salvaItem(item);
    bloc.remove();
  }

  _editarItem(Item item){
    Dialogs dialogs = new Dialogs();
    dialogs.editarItem(context, item);
  }

  Widget _listaItens(ItemBloc bloc){
    return StreamBuilder(
        stream: bloc.outList,
        builder: (BuildContext context, AsyncSnapshot<List<Item>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                itemBuilder: (context, index) {
                  if (index < snapshot.data.length) {
                    return Column(
                      children: <Widget>[
                        GestureDetector(
                          child: ListTile(
                            title: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                /*Checkbox(
                                    value: true,
                                    onChanged: (bool value){
                                      snapshot.data.elementAt(index).checado = value;
                                    }
                                )
                                ,*/
                                Expanded(
                                  child: Text(
                                      snapshot.data.elementAt(index).nome
                                  ),
                                ),
                                Expanded(
                                  child: Icon(
                                      snapshot.data.elementAt(index).par ? Icons.people : Icons.people_outline
                                  ),
                                ),
                                Expanded(
                                  child: Text(
                                      snapshot.data.elementAt(index).quantidade.toString()
                                  ),
                                )
                              ],
                            ),
                            onTap: () => _editarItem(snapshot.data.elementAt(index)),
                          ),
                          onLongPress: () =>
                              _removeItem(bloc, snapshot.data.elementAt(index)),
                        ),
                        Divider(),
                      ],
                    );
                  }
                }
            );
          }else{
            return Divider();
          }
        }
    );
  }

  @override
  Widget build(BuildContext context) {

    itemBloc.carregaLista(viagemBloc.viagemValue.id);

    return Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(
              color: Colors.black87
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            viagemBloc.nomeValue,
            style: TextStyle(
                color: Colors.black87
            ),
          )
      ),
      body: _listaItens(itemBloc),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(width: 20), //Espaço em branco
            IconButton(
                alignment: Alignment.center,
                icon: Icon(Icons.add_box),
                onPressed: (){
                  _cadastroItem(viagemBloc.viagemValue.id);
                }
            ),
            SizedBox(width: 20) //Espaço em branco
          ],
        ),
      ),
    );
  }
}