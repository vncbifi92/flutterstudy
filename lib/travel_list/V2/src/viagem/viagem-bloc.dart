import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:study/travel_list/V2/src/shared/service/database.dart';
import 'package:study/travel_list/V2/src/viagem/viagem-model.dart';

class ViagemBloc extends BlocBase{

  DatabaseHelper databaseHelper = DatabaseHelper.internal();

  var _nome = BehaviorSubject<String>.seeded("");

  Stream<String> get outNome => _nome.stream;

  Sink<String> get inNome => _nome.sink;

  String get nomeValue => _nome.value;

  void salvaNome(String nome){
    inNome.add(nome);
    print("nome");
  }

  var _viagem = BehaviorSubject<Viagem>.seeded(Viagem(""));

  Stream<Viagem> get outViagem => _viagem.stream;

  Sink<Viagem> get inViagem => _viagem.sink;

  Viagem get viagemValue => _viagem.value;

  void salvaViagem(Viagem viagem){
    inViagem.add(viagem);
    print("viagem");
  }

  var _listViagem = BehaviorSubject<List<Viagem>>.seeded(List<Viagem>());

  Stream<List<Viagem>> get outList => _listViagem.stream;

  Sink<List<Viagem>> get inList => _listViagem.sink;

  List<Viagem> get listViagemValue => _listViagem.value;

  @override
  void dispose() {
    _nome.close();
    _viagem.close();
    _listViagem.close();
  }

  void carregaLista() async{
    List<Viagem> viagens = await databaseHelper.getViagens();
    viagens.sort((a, b) {
      return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
    });
    _listViagem.value = viagens;
  }

  void add() async {
    Viagem value = Viagem(_nome.value);
    await databaseHelper.insertViagem(value);
    _listViagem.value.add(value);
    _listViagem.value.sort((a, b) {
      return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
    });
    inList.add(_listViagem.value);
    print("salva lista");
  }

  void edit(){
    Viagem value = _viagem.value;
    value.nome = _nome.value;
    databaseHelper.updateViagem(value);
    _listViagem.value.sort((a, b) {
      return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
    });
    inList.add(_listViagem.value);
  }

  void remove(){
    databaseHelper.deleteViagem(_viagem.value.id);
    _listViagem.value.remove(_viagem.value);
    inList.add(_listViagem.value);
    print("remove lista");
  }
}

ViagemBloc viagemBloc = ViagemBloc();