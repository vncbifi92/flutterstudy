import 'package:flutter/material.dart';
import 'package:study/travel_list/V2/src/viagem/viagem-bloc.dart';

class Dialogs{

  cadastroViagem(BuildContext context) {

    final formKey = GlobalKey<FormState>();

    _salvaViagem(ViagemBloc bloc) {
      final form = formKey.currentState;
      if (form.validate()) {
        form.save();
        bloc.add();
        Navigator.of(context).pop();
      }
    }

    Widget _okButton(ViagemBloc bloc){
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Confirmar",
          style: TextStyle(
              color: Colors.orange
          ),
        ),
        onPressed: (){_salvaViagem(bloc);},
      );
    }

    Widget _cancelButton(){
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Cancelar",
          style: TextStyle(
              color: Colors.orange
          ),
        ),
        onPressed: (){Navigator.pop(context);},
      );
    }

    Widget _viagemForm(ViagemBloc bloc){
      return Form(
          key: formKey,
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: TextFormField(
              onSaved: (String val){bloc.salvaNome(val);},
              decoration: InputDecoration(
                  labelText: "Nome",
                  border: OutlineInputBorder()),
            ),
          )
      );
    }

    return  showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text("Iniciar uma nova viagem"),
            content: _viagemForm(viagemBloc),
            actions: <Widget>[
              _cancelButton(),
              _okButton(viagemBloc)
            ],
          );
        }
    );
  }
}