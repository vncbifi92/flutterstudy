import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:study/travel_list/V2/src/shared/routes.dart';
import 'package:study/travel_list/V2/src/viagem/viagem-bloc.dart';
import 'package:study/travel_list/V2/src/viagem/viagem-widget.dart';

class AppWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    viagemBloc.carregaLista();

    return MaterialApp(
        routes: routes,
        debugShowCheckedModeBanner: false,
        home: ViagemPage()
    );
  }
}