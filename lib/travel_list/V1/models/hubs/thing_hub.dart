import 'package:study/travel_list/V1/models/pojos/thing.dart';

/*{
	"things": [{
		"id": 1,
		"name": "teste",
		"quantity": 5,
		"isPair": true,
		"idTravel": 1
	}]
}*/
class ThingHub {
  List<Thing> _things;

  ThingHub({List<Thing> things}) {
    this._things = things;
  }

  List<Thing> get things => _things;
  set things(List<Thing> things) => _things = things;

  ThingHub.fromJson(Map<String, dynamic> json) {
    if (json['things'] != null) {
      _things = new List<Thing>();
      json['things'].forEach((v) {
        _things.add(new Thing.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._things != null) {
      data['things'] = this._things.map((v) => v.toJson()).toList();
    }
    return data;
  }

  ThingHub.fromMap(Map<String, dynamic> map) {
    if (map['things'] != null) {
      _things = new List<Thing>();
      map['things'].forEach((v) {
        _things.add(new Thing.fromMap(v));
      });
    }
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._things != null) {
      data['things'] = this._things.map((v) => v.toMap()).toList();
    }
    return data;
  }
}