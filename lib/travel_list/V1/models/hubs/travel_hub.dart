import 'package:study/travel_list/V1/models/pojos/travel.dart';

/*{
	"travels": [{V1/
		"id": 1,
		"name": "viagem 1",
		"thingHub": {
			"things": [{
				"id": 1,
				"name": "teste",
				"quantity": 5,
				"isPair": true,
				"idTravel": 1
			}]
		}
	}]
}*/

class TravelHub {
  List<Travel> _travels;

  TravelHub({List<Travel> travels}) {
    this._travels = travels;
  }

  List<Travel> get travels => _travels;
  set travels(List<Travel> travels) => _travels = travels;

  TravelHub.fromJson(Map<String, dynamic> json) {
    if (json['travels'] != null) {
      _travels = new List<Travel>();
      json['travels'].forEach((v) {
        _travels.add(new Travel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._travels != null) {
      data['travels'] = this._travels.map((v) => v.toJson()).toList();
    }
    return data;
  }

  TravelHub.fromMap(Map<String, dynamic> map) {
    if (map['travels'] != null) {
      _travels = new List<Travel>();
      map['travels'].forEach((v) {
        _travels.add(new Travel.fromMap(v));
      });
    }
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._travels != null) {
      data['travels'] = this._travels.map((v) => v.toJson()).toList();
    }
    return data;
  }
}