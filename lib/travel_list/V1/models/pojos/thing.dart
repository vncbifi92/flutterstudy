/* {
    "id": 1,
    "name": "teste",
    "quantity": 5,
    "isPair": true,
    "idTravel": 1
 }*/
class Thing {
  int _id;
  String _name;
  int _quantity;
  bool _isPair;
  int _idTravel;

  Thing({int id, String name, int quantity, bool isPair, int idTravel}) {
    this._id = id;
    this._name = name;
    this._quantity = quantity;
    this._isPair = isPair;
    this._idTravel = idTravel;
  }

  int get id => _id;

  set id(int id) => _id = id;

  String get name => _name;

  set name(String name) => _name = name;

  int get quantity => _quantity;

  set quantity(int quantity) => _quantity = quantity;

  bool get isPair => _isPair;

  set isPair(bool isPair) => _isPair = isPair;

  int get idTravel => _idTravel;

  set idTravel(int idTravel) => _idTravel = idTravel;

  Thing.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _quantity = json['quantity'];
    _isPair = json['isPair'];
    _idTravel = json['idTravel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['quantity'] = this._quantity;
    data['isPair'] = this._isPair;
    data['idTravel'] = this._idTravel;
    return data;
  }

  Thing.fromMap(Map<String, dynamic> map) {
    _id = map['id'];
    _name = map['name'];
    _quantity = map['quantity'];
    _isPair = map['isPair'] == 1;
    _idTravel = map['idTravel'];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'name': this._name,
      'quantity': this._quantity,
      'isPair': this._isPair == true ? 1 : 0
    };
    if (_id != null) {
      map["id"] = _id;
    }
    if (_idTravel != null) {
      map["idTravel"] = _idTravel;
    }
    return map;
  }
}
