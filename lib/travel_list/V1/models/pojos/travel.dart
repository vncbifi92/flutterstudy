import 'package:study/travel_list/V1/models/hubs/thing_hub.dart';

/*{
	"id": 1,
	"name": "viagem 1",
	"thingHub": {
		"things": [{
			"id": 1,
			"name": "teste",
			"quantity": 5,
			"isPair": true,
			"idTravel": 1
		}]
	}
}*/

class Travel {
  int _id;
  String _name;
  ThingHub _thingHub;

  Travel({int id, String name, ThingHub thingHub}) {
    this._id = id;
    this._name = name;
    this._thingHub = thingHub;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  ThingHub get thingHub => _thingHub;
  set thingHub(ThingHub thingHub) => _thingHub = thingHub;

  Travel.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _thingHub = json['thingHub'] != null
        ? new ThingHub.fromJson(json['thingHub'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    if (this._thingHub != null) {
      data['thingHub'] = this._thingHub.toJson();
    }
    return data;
  }

  Travel.fromMap(Map<String, dynamic> map) {
    _id = map['id'];
    _name = map['name'];
    _thingHub = map['thingHub'] != null
        ? new ThingHub.fromMap(map['thingHub'])
        : null;
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (_id != null) {
      data["id"] = _id;
    }
    data['name'] = this._name;
    /*if (this._thingHub != null) {
      data['thingHub'] = this._thingHub.toMap();
    }*/
    return data;
  }
}