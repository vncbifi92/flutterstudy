import 'package:flutter/material.dart';
import 'package:study/travel_list/V1/screens/home_screen.dart';
import 'package:study/travel_list/V1/screens/thing/edit_thing_screen.dart';
import 'package:study/travel_list/V1/screens/thing/new_thing_screen.dart';
import 'package:study/travel_list/V1/screens/thing/things_screen.dart';
import 'package:study/travel_list/V1/screens/travel/edit_travel_screen.dart';
import 'package:study/travel_list/V1/screens/travel/travels_screen.dart';
import 'package:study/travel_list/V1/screens/travel/new_travel_screen.dart';

final routes = {
  //'/': (BuildContext context) => new HomePage(title: "Home"),
  '/': (BuildContext context) => new TravelsPage(title: "Places"),
  '/travels/new_travel': (BuildContext context) => new NewTravelPage(title: "New Place"),
  '/travels/edit_travel': (BuildContext context) => new EditTravelPage(title: "Edit Place"),
  '/things': (BuildContext context) => new ThingsPage(title: "Stuffs"),
  '/things/new_thing': (BuildContext context) => new NewThingPage(title: "New Stuff"),
  '/things/edit_thing': (BuildContext context) => new EditThingPage(title: "Edit Stuff"),
};