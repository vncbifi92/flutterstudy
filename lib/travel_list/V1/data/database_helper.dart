import 'dart:async';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:study/travel_list/V1/models/hubs/thing_hub.dart';
import 'package:study/travel_list/V1/models/hubs/travel_hub.dart';
import 'package:study/travel_list/V1/models/pojos/thing.dart';
import 'package:study/travel_list/V1/models/pojos/travel.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if(_db != null){
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "main.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table

    await db.execute(
        "CREATE TABLE Travel( id INTEGER PRIMARY KEY AUTOINCREMENT"
            ", name TEXT )");

    await db.execute(
        "CREATE TABLE Thing( id INTEGER PRIMARY KEY AUTOINCREMENT"
            ", name TEXT"
            ", quantity INTEGER"
            ", isPair NUMERIC"
            ", idTravel NUMERIC "
            ", FOREIGN KEY (idTravel) REFERENCES Travel(id) )");
  }

  Future closeDb() async {
    var dbClient = await db;
    dbClient.close();
  }

  Future<Thing> getThing(int id) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Thing",
        columns: ["id", "name", "quantity", "isPair", "idTravel"],
        where: "id = ?",
        whereArgs: [id]);
    if (maps.length > 0) {
      return new Thing.fromMap(maps.first);
    }
    return null;
  }

  Future<Thing> insertThing(Thing thing) async {
    var dbClient = await db;
    thing.id = await dbClient.insert("Thing", thing.toMap());
    return thing;
  }

  Future<int> deleteThing(int id) async {
    var dbClient = await db;
    return await dbClient.delete("Thing", where: "id = ?", whereArgs: [id]);
  }

  Future<int> updateThing(Thing thing) async {
    var dbClient = await db;
    return await dbClient.update("Thing", thing.toMap(),
        where: "id = ?", whereArgs: [thing.id]);
  }

  Future<ThingHub> getTravelsThingHub(int idTravel) async {
    List<Thing> things = new List<Thing>();
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Thing",
        columns: ["id", "name", "quantity", "isPair"],
        where: "idTravel = ?",
        whereArgs: [idTravel]);
    if (maps.length > 0) {
      for (Map thing in maps) {
        things.add(Thing.fromMap(thing));
      }
      return ThingHub(things: things);
    }
    return null;
  }

  Future<Travel> getTravel(int id) async {
    var dbClient = await db;
    Travel travel;
    List<Map> maps = await dbClient.query("Travel",
        columns: ["id", "name"],
        where: "id = ?",
        whereArgs: [id]);
    if (maps.length > 0) {
      travel = new Travel.fromMap(maps.first);

      travel.thingHub = await getTravelsThingHub(id);

      return travel;
    }
    return null;
  }

  Future<Travel> insertTravel(Travel travel) async {
    var dbClient = await db;
    travel.id = await dbClient.insert("Travel", travel.toMap());
    if(travel.thingHub != null && travel.thingHub.things.length > 0) {
      for (Thing thing in travel.thingHub.things) {
        thing.idTravel = travel.id;
        await updateThing(thing);
      }
    }
    return travel;
  }

  Future<int> deleteTravel(Travel travel) async {
    var dbClient = await db;

    if(travel.thingHub != null && travel.thingHub.things.length > 0) {
      for (Thing thing in travel.thingHub.things) {
        await deleteThing(thing.id);
      }
    }

    return await dbClient.delete("Travel", where: "id = ?", whereArgs: [travel.id]);
  }

  Future<int> updateTravel(Travel travel) async {
    var dbClient = await db;

    if(travel.thingHub != null && travel.thingHub.things.length > 0) {
      for (Thing thing in travel.thingHub.things) {
        thing.idTravel = travel.id;
        await updateThing(thing);
      }
    }

    return await dbClient.update("Travel", travel.toMap(),
        where: "id = ?", whereArgs: [travel.id]);
  }

  Future<TravelHub> getTravelHub() async {
    List<Travel> travels = new List<Travel>();
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Travel");
    if (maps.length > 0) {
      for (Map travel in maps) {
        Travel _travel = Travel.fromMap(travel);
        _travel.thingHub = await getTravelsThingHub(_travel.id);
        travels.add(_travel);
      }
      return TravelHub(travels: travels);
    }
    return null;
  }

}