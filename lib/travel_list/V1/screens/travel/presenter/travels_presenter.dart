import 'package:study/travel_list/V1/data/database_helper.dart';
import 'package:study/travel_list/V1/models/pojos/travel.dart';
import 'package:study/travel_list/V1/models/hubs/travel_hub.dart';

abstract class TravelScreenContract {

  void onDeleteSuccess();
  void onUpdateSuccess();
  void onInsertSuccess();
  void onGetTravel(Travel travel);
  void onGetTravelHub(TravelHub travelHub);

}

class TravelScreenPresenter{
  TravelScreenContract _view;
  DatabaseHelper databaseHelper = DatabaseHelper.internal();

  TravelScreenPresenter(this._view);

  doInsert(Travel travel){
    databaseHelper.insertTravel(travel).then((Travel travel){
      _view.onInsertSuccess();
    });
  }

  doDelete(Travel travel){
    databaseHelper.deleteTravel(travel).then((int qtd){
      _view.onDeleteSuccess();
    });
  }

  doUpdate(Travel travel){
    databaseHelper.updateTravel(travel).then((int qtd){
      _view.onUpdateSuccess();
    });
  }

  doGetTravel(int id){
    databaseHelper.getTravel(id).then((Travel travel){
      _view.onGetTravel(travel);
    });
  }

  doGetTravelHub(){
    databaseHelper.getTravelHub().then((TravelHub travelHub){
      _view.onGetTravelHub(travelHub);
    });
  }

}