import 'package:flutter/material.dart';
import 'package:study/travel_list/V1/models/hubs/thing_hub.dart';
import 'package:study/travel_list/V1/models/hubs/travel_hub.dart';
import 'package:study/travel_list/V1/models/pojos/travel.dart';
import 'package:study/travel_list/V1/screens/travel/presenter/travels_presenter.dart';

class EditTravelPage extends StatefulWidget {
  EditTravelPage({Key key, this.title, this.travel}) : super(key: key);

  final String title;
  final Travel travel;

  @override
  _EditTravelPageState createState() => new _EditTravelPageState(travel);
}

class _EditTravelPageState extends State<EditTravelPage> implements TravelScreenContract{

  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  String _name;
  ThingHub _thingHub;
  TravelScreenPresenter _presenter;

  _EditTravelPageState(Travel travel){
    _presenter = TravelScreenPresenter(this);
    _name = travel.name;
    _thingHub = travel.thingHub;
  }

  _showSnackBar(String text){
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(text)));
  }

  @override
  void onDeleteSuccess() {
    //DUMMY
  }

  @override
  void onGetTravel(Travel travel) {
    //DUMMY
  }

  @override
  void onGetTravelHub(TravelHub travelHub) {
    //DUMMY
  }

  @override
  void onInsertSuccess() {
    //DUMMY
  }

  @override
  void onUpdateSuccess() {
    Navigator.of(context).pop();
  }

  _goToThings()async{
    await Navigator.of(context).pushNamed('/things').then((obj) {
      _thingHub = obj;
      print(_thingHub.toMap().toString());
    });
  }

  _saveTravel(){
    final form = formKey.currentState;
    if(form.validate()){
      form.save();
      Travel travel = Travel(id: widget.travel.id, name: _name, thingHub: _thingHub);
      _presenter.doUpdate(travel);
    }
  }

  Widget _okButton(){
    return MaterialButton(
      color: Colors.primaries[0],
      child: Text("Save",
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      onPressed: _saveTravel,
    );
  }

  Widget _thingsButton(){
    return MaterialButton(
      color: Colors.primaries[0],
      child: Text("Manage Thing Hub",
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      onPressed: _goToThings,
    );
  }

  Widget _travelForm(){
    return Column(
      children: <Widget>[
        Text(
          "Place",
          textScaleFactor: 2.0,
        ),
        Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    initialValue: widget.travel.name,
                    onSaved: (String val) {
                      setState(() {
                        _name = val;
                      });
                    },
                    decoration: new InputDecoration(labelText: "Name"),
                  ),
                ),
              ],
            )
        ),
        _thingsButton(),
        _okButton()
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      key: scaffoldKey,
      body: new Container(
        child: new Center(
          child: new Container(
            child: _travelForm(),
            height: 400.0,
            width: 300.0,
          ),
        ),
      ),
    );
  }

}