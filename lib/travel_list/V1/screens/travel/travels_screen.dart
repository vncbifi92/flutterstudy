import 'package:flutter/material.dart';
import 'package:study/travel_list/V1/models/hubs/travel_hub.dart';
import 'package:study/travel_list/V1/models/pojos/travel.dart';
import 'package:study/travel_list/V1/screens/travel/edit_travel_screen.dart';
import 'package:study/travel_list/V1/screens/travel/presenter/travels_presenter.dart';

class TravelsPage extends StatefulWidget {
  TravelsPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TravelsPageState createState() => new _TravelsPageState();
}

class _TravelsPageState extends State<TravelsPage> implements TravelScreenContract{
  final scaffoldKey = GlobalKey<ScaffoldState>();
  TravelScreenPresenter _presenter;
  ListView travelList =  ListView.builder(
      itemBuilder: (context, i) {
        if (i == 0) return Text("None travel yet!");
      }
  );

  @override
  void onDeleteSuccess() {
    _presenter.doGetTravelHub();
  }

  @override
  void onGetTravel(Travel travel) {
    //DUMMY
  }

  @override
  void onGetTravelHub(TravelHub travelHub) {
    _getTravelList(travelHub);
  }

  @override
  void onInsertSuccess() {
    //DUMMY
  }

  @override
  void onUpdateSuccess() {
    //DUMMY
  }

  _TravelsPageState(){
    _presenter = TravelScreenPresenter(this);
    _presenter.doGetTravelHub();
  }

  _showSnackBar(String text){
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(text)));
  }

  _deleteTravel(Travel travel){
    _presenter.doDelete(travel);
  }

  _editTravel(Travel travel){
    Navigator.of(context).push(
        new MaterialPageRoute(
            settings: const RouteSettings(name: '/travel/edit_travel'),
            builder: (context) => new EditTravelPage(
              travel: travel,
              title: "Edit Place",
            )
        )
    ).then((obj){
      _presenter.doGetTravelHub();
    });;
  }

  _newTravel(){
    Navigator.of(context).pushNamed('/travels/new_travel').then((obj) {
      _presenter.doGetTravelHub();
    });
  }

  _getTravelList(TravelHub travelHub) {
    if (travelHub == null ? false : travelHub.travels != null &&
        travelHub.travels.length > 0) {
      setState(() {

        travelList = ListView.builder(
            itemBuilder: (context, index) {
              if (index < travelHub.travels.length) {
                return Column(
                  children: <Widget>[
                    GestureDetector(
                      child: ListTile(
                        title: Text(
                            travelHub.travels
                                .elementAt(index)
                                .name
                                .toString()
                        ),
                        onTap: () => _editTravel(travelHub.travels.elementAt(index)),
                      ),
                      onLongPress: () =>
                          _deleteTravel(travelHub.travels.elementAt(index)),
                    ),
                    Divider()
                  ],
                );
              }
            }
        );

      });
    } else {
      setState(() {
        travelList =  ListView.builder(
            itemBuilder: (context, i) {
              if (i == 0) return Text("None travel yet!");
            }
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      key: scaffoldKey,
      body: Card(
        child: Container(
            padding: EdgeInsets.all(10.0),
            child: travelList
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
                Radius.circular(25.0)
            ),
            side: BorderSide.none
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: _newTravel
      ),
    );
  }
}