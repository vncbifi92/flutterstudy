import 'package:study/travel_list/V1/models/hubs/thing_hub.dart';
import 'package:study/travel_list/V1/models/pojos/thing.dart';
import 'package:flutter/material.dart';
import 'package:study/travel_list/V1/screens/thing/edit_thing_screen.dart';
import 'package:study/travel_list/V1/screens/thing/presenter/things_presenter.dart';

class ThingsPage extends StatefulWidget {
  ThingsPage({Key key, this.title, this.idTravel}) : super(key: key);

  final String title;
  final int idTravel;

  @override
  _ThingsPageState createState() => new _ThingsPageState();
}

class _ThingsPageState extends State<ThingsPage> implements ThingsScreenContract{

  final scaffoldKey = GlobalKey<ScaffoldState>();
  ThingsScreenPresenter _presenter;
  ThingHub _thingHub;
  ListView thingList =  ListView.builder(
      itemBuilder: (context, i) {
        if (i == 0) return Text("None stuff yet!");
      }
  );

  _ThingsPageState(){
    _presenter = ThingsScreenPresenter(this);
    if(widget.idTravel != null) {
      _presenter.doGetThingHub(widget.idTravel);
    }
  }

  @override
  void onInsertSuccess(){
    //DUMMY
  }

  @override
  void onGetThing(Thing thing) {
    //DUMMY
  }

  @override
  void onUpdateSuccess() {
    //DUMMY
  }

  @override
  void onDeleteSuccess() {
    if(widget.idTravel != null) {
      _presenter.doGetThingHub(widget.idTravel);
    }
  }

  @override
  void onGetThingHub(ThingHub thingHub){
    _getThingsList(thingHub);
  }

  _showSnackBar(String text){
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(text)));
  }

  _deleteThing(Thing thing){
    _presenter.doDelete(thing);
  }

  _newThing(){
    Navigator.of(context).pushNamed('/things/new_thing').then((obj) {
      if(widget.idTravel != null) {
        _presenter.doGetThingHub(widget.idTravel);
      }
    });
  }

  _editThing(Thing thing){
    Navigator.of(context).push(
        new MaterialPageRoute(
            settings: const RouteSettings(name: '/things/edit_thing'),
            builder: (context) => new EditThingPage(
              thing: thing,
              title: "Edit Stuff",
            )
        )
    ).then((obj){
      if(widget.idTravel != null) {
        _presenter.doGetThingHub(widget.idTravel);
      }
    });
  }

  _getThingsList(ThingHub thingHub) {
    if (thingHub == null ? false : thingHub.things != null &&
        thingHub.things.length > 0) {
      _thingHub = thingHub;
      setState(() {
        thingList = ListView.builder(
            itemBuilder: (context, index) {
              if (index < thingHub.things.length) {
                return Column(
                  children: <Widget>[
                    GestureDetector(
                      child: ListTile(
                        title: Text(
                            thingHub.things
                                .elementAt(index)
                                .name
                                .toString()
                        ),
                        subtitle: Text(
                            "Quantity: " + thingHub.things
                                .elementAt(index)
                                .quantity
                                .toString()
                                + (thingHub.things
                                .elementAt(index)
                                .isPair ? " pairs" : "")
                        ),
                        onTap: () => _editThing(thingHub.things.elementAt(index)),
                      ),
                      onLongPress: () =>
                          _deleteThing(thingHub.things.elementAt(index)),
                    ),
                    Divider()
                  ],
                );
              }
            }
        );
      });
    } else {
      setState(() {
        thingList = ListView.builder(
            itemBuilder: (context, i) {
              if (i == 0) return Text("None stuff yet!");
            }
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: (){Navigator.pop(context,(_thingHub!=null ? _thingHub : new ThingHub()));}
        ),
      ),
      key: scaffoldKey,
      body: Card(
        child: Container(
            padding: EdgeInsets.all(10.0),
            child: thingList
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
                Radius.circular(25.0)
            ),
            side: BorderSide.none
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: _newThing
      ),
    );
  }
}