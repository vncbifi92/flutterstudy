import 'package:study/travel_list/V1/models/hubs/thing_hub.dart';
import 'package:study/travel_list/V1/models/pojos/thing.dart';
import 'package:flutter/material.dart';
import 'package:study/travel_list/V1/screens/thing/presenter/things_presenter.dart';

class EditThingPage extends StatefulWidget {
  EditThingPage({Key key, this.title, this.thing}) : super(key: key);

  final String title;
  final Thing thing;

  @override
  _EditThingPageState createState() => new _EditThingPageState(thing);
}

class _EditThingPageState extends State<EditThingPage> implements ThingsScreenContract{
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  String _name;
  int _quantity = 0;
  bool _isPair = false;
  ThingsScreenPresenter _presenter;

  _EditThingPageState(Thing thing){
    _presenter = ThingsScreenPresenter(this);
    _name = thing.name;
    _quantity = thing.quantity;
    _isPair = thing.isPair;
  }

  _showSnackBar(String text){
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(text)));
  }

  @override
  void onInsertSuccess(){
    //DUMMY
  }

  @override
  void onDeleteSuccess() {
    //DUMMY
  }

  @override
  void onGetThingHub(ThingHub thingHub) {
    //DUMMY
  }

  @override
  void onGetThing(Thing thing) {
    //DUMMY
  }

  @override
  void onUpdateSuccess() {
    Navigator.of(context).pop();
  }

  _saveThing(){
    final form = formKey.currentState;
    if(form.validate()){
      form.save();
      Thing thing = Thing(id: widget.thing.id, name: _name, isPair: _isPair, quantity: _quantity);
      _presenter.doUpdate(thing);
    }
  }

  Widget _okButton(){
    return MaterialButton(
      color: Colors.primaries[0],
      child: Text("Save",
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      onPressed: _saveThing,
    );
  }

  Widget _thingForm() {
    return Column(
      children: <Widget>[
        Text(
          "Stuff",
          textScaleFactor: 2.0,
        ),
        Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    initialValue: widget.thing.name,
                    onSaved: (String val) {
                      setState(() {
                        _name = val;
                      });
                    },
                    decoration: new InputDecoration(labelText: "Name"),
                  ),
                ),
                Text("Quantity"),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Slider(
                    value: _quantity.toDouble(),
                    min: 1.0,
                    max: 100.0,
                    divisions: 100,
                    label: _quantity.toString(),
                    onChanged: (double val) {
                      setState(() {
                        _quantity = val.round();
                      });
                    },
                  ),
                ),
                Row(
                  children: <Widget>[
                    Text("Is Pair?"),
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Checkbox(
                          value: _isPair,
                          onChanged: (val) {
                            setState(() {
                              _isPair = val; //1 - true; 0 - false;
                            });
                          },
                        )
                    ),
                  ],
                )
              ],
            )
        ),
        _okButton()
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      key: scaffoldKey,
      body: new Container(
        child: new Center(
          child: new Container(
            child: _thingForm(),
            height: 400.0,
            width: 300.0,
          ),
        ),
      ),
    );
  }

}