import 'package:study/travel_list/V1/data/database_helper.dart';
import 'package:study/travel_list/V1/models/pojos/thing.dart';
import 'package:study/travel_list/V1/models/hubs/thing_hub.dart';

abstract class ThingsScreenContract {

  void onDeleteSuccess();
  void onUpdateSuccess();
  void onInsertSuccess();
  void onGetThing(Thing thing);
  void onGetThingHub(ThingHub thingHub);

}

class ThingsScreenPresenter{
  ThingsScreenContract _view;
  DatabaseHelper databaseHelper = DatabaseHelper.internal();

  ThingsScreenPresenter(this._view);

  doInsert(Thing thing){
    databaseHelper.insertThing(thing).then((Thing thing){
      _view.onInsertSuccess();
    });
  }

  doDelete(Thing thing){
    databaseHelper.deleteThing(thing.id).then((int qtd){
      _view.onDeleteSuccess();
    });
  }

  doUpdate(Thing thing){
    databaseHelper.updateThing(thing).then((int qtd){
      _view.onUpdateSuccess();
    });
  }

  doGetThing(int id){
    databaseHelper.getThing(id).then((Thing thing){
      _view.onGetThing(thing);
    });
  }

  doGetThingHub(int idTravel){
    databaseHelper.getTravelsThingHub(idTravel).then((ThingHub thingHub){
      _view.onGetThingHub(thingHub);
    });
  }

}