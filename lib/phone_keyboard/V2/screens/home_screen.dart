import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage>{

  String _output = "";

  _deleteAll(){
    setState(() {
      _output = "";
    });
  }

  _backspace(){
    setState(() {
      if(_output.length == 1){
        _output = "";
      }else if(_output.length > 0) {
        _output = _output.substring(0, _output.length - 1);
      }
    });
  }

  _buttonPressed(String buttonText){
    setState(() {
      if(_output.length < 42) {
        _output = _output + buttonText;
      }
    });
  }

  _buttonCallPressed() async {
    if(await canLaunch("tel:+55" + _output)) {
      setState(() {
        launch("tel:+55" + _output);
      });
    }
  }

  _buttonSmsPressed() async {
    if(await canLaunch("sms:+55" + _output)) {
      setState(() {
        launch("sms:+55" + _output);
      });
    }
  }

  Widget _buildIconButton(IconData icon, Color backgroundColor, Color iconColor, dynamic function){
    return Expanded(
      child: MaterialButton(

        padding: EdgeInsets.all(
            24.0
        ),
        child: Icon(
          icon,
          color: iconColor,
        ),
        color: backgroundColor,
        onPressed: function,
      ),
    );
  }

  Widget _buildTextButton(String buttonText, Color backgroundColor, Color textColor, dynamic function){
    return Expanded(
      child: MaterialButton(
        color: backgroundColor,
        padding: EdgeInsets.all(
            24.0
        ),
        child: Text(
          buttonText,
          style: TextStyle(
              fontSize: 28.0,
              fontWeight: FontWeight.bold,
              color: textColor
          ),
        ),
        onPressed: () => function(buttonText),
      ),
    );
  }

  Widget _buildBackspace(){
    return Container(
      alignment: Alignment.bottomRight,
      child: GestureDetector(
        child: MaterialButton(
            child: Icon(
              Icons.backspace
            ),
            onPressed:  () => _backspace()
        ),
        onLongPress: () => _deleteAll(),
      ),
    );
  }

  Widget _buildDisplay(){
    return Expanded(
      child: Container(
        alignment: Alignment.centerRight,
        padding: EdgeInsets.symmetric(
            vertical: 24.0,
            horizontal: 12.0
        ),
        child: Text(
          _output,
          style: TextStyle(
              fontSize: 48.0,
              fontWeight: FontWeight.bold
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Container(
          child: Column(
            children: <Widget>[
              _buildDisplay(),

              _buildBackspace(),

              Divider(),

              Column(
                children: [
                  Row(
                    children: [
                      _buildTextButton("1", Colors.white, Colors.black, _buttonPressed),
                      _buildTextButton("2", Colors.white, Colors.black, _buttonPressed),
                      _buildTextButton("3", Colors.white, Colors.black, _buttonPressed),
                    ],
                  ),
                  Row(
                    children: [
                      _buildTextButton("4", Colors.white, Colors.black, _buttonPressed),
                      _buildTextButton("5", Colors.white, Colors.black, _buttonPressed),
                      _buildTextButton("6", Colors.white, Colors.black, _buttonPressed),
                    ],
                  ),
                  Row(
                    children: [
                      _buildTextButton("7", Colors.white, Colors.black, _buttonPressed),
                      _buildTextButton("8", Colors.white, Colors.black, _buttonPressed),
                      _buildTextButton("9", Colors.white, Colors.black, _buttonPressed),
                    ],
                  ),
                  Row(
                    children: [
                      _buildTextButton("*", Colors.white, Colors.black, _buttonPressed),
                      _buildTextButton("0", Colors.white, Colors.black, _buttonPressed),
                      _buildTextButton("#", Colors.white, Colors.black, _buttonPressed),
                    ],
                  ),
                  Row(
                    children: [
                      _buildIconButton(Icons.call, Colors.green, Colors.white, _buttonCallPressed),
                      _buildIconButton(Icons.message, Colors.deepOrange, Colors.white, _buttonSmsPressed)
                    ],
                  )
                ],
              )
            ],
          )
      ),
    );
  }

}