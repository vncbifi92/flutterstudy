import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'PhoneKeyboard',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'PhoneKeyboard'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String output = "";

  deleteAll(){
    setState(() {
      output = "";
    });
  }

  backspace(){
    setState(() {
      if(output.length == 1){
        output = "";
      }else {
        output = output.substring(0, output.length - 1);
      }
    });
  }

  buttonPressed(String buttonText){
    setState(() {
      if(output.length < 42) {
        output = output + buttonText;
      }
    });
  }

  Widget buildButton(String buttonText){
    return Expanded(
      child: MaterialButton(
        padding: EdgeInsets.all(
            24.0
        ),
        child: Text(
          buttonText,
          style: TextStyle(
              fontSize: 28.0,
              fontWeight: FontWeight.bold
          ),
        ),
        onPressed: () => buttonPressed(buttonText),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Container(
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.symmetric(
                      vertical: 24.0,
                      horizontal: 12.0
                  ),
                  child: Text(
                    output,
                    style: TextStyle(
                        fontSize: 48.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),

              Container(
                alignment: Alignment.bottomRight,
                child: GestureDetector(
                  child: MaterialButton(
                      child: Icon(
                          Icons.backspace
                      ),
                      onPressed:  () => backspace()
                  ),
                  onLongPress: () => deleteAll(),
                ),
              ),

              Divider(),

              Column(
                children: [
                  Row(
                    children: [
                      buildButton("1"),
                      buildButton("2"),
                      buildButton("3"),
                    ],
                  ),
                  Row(
                    children: [
                      buildButton("4"),
                      buildButton("5"),
                      buildButton("6"),
                    ],
                  ),
                  Row(
                    children: [
                      buildButton("7"),
                      buildButton("8"),
                      buildButton("9"),
                    ],
                  ),
                  Row(
                    children: [
                      buildButton("*"),
                      buildButton("0"),
                      buildButton("#"),
                    ],
                  )
                ],
              )
            ],
          )
      ),
    );
  }
}