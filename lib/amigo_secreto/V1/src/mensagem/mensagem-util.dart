import 'dart:math';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server/gmail.dart';
import 'package:study/amigo_secreto/V1/src/pessoa/pessoa-model.dart';
import 'package:study/amigo_secreto/V1/src/profile/profile-bloc.dart';


class MensagemUtil{

  sortearAmigos(List<Pessoa>_listaPessoas){

    /*List<Pessoa> listaSorteio = _listaPessoas;
    var rng = new Random();
    _listaPessoas.forEach(
      (pessoa){
        listaSorteio.remove(pessoa);
        Pessoa _sorteado = listaSorteio.elementAt(rng.nextInt(listaSorteio.length));

        _enviaEmail(profileBloc.emailValue, "Bifi1992", _sorteado.nome, _sorteado.email);

        listaSorteio.add(pessoa);
        listaSorteio.remove(_sorteado);
      }
    );*/

    List<Pessoa> listaSorteio = new List<Pessoa>.from(_listaPessoas);
    var rng = new Random();
    List<Pessoa> sorteados = new List<Pessoa>();
    _listaPessoas.forEach(
            (pessoa){
          listaSorteio.remove(pessoa);
          Pessoa _sorteado = listaSorteio.elementAt(rng.nextInt(listaSorteio.length));
          print('de: $pessoa | para: $_sorteado');
          if(!sorteados.contains(pessoa)){
            listaSorteio.add(pessoa);
          }
          _enviaEmail(profileBloc.emailValue, "Bifi1992", _sorteado.nome, pessoa.email);
          listaSorteio.remove(_sorteado);
          sorteados.add(_sorteado);
        }
    );

  }

  _enviaEmail(String _email, String _senha, String _amigo, String _amigoEmail)async {

    final smtpServer = gmail(_email, _senha);

    final equivalentMessage = new Message()
      ..from = new Address(_email, 'no-reply')
      ..recipients.add(_amigoEmail)
      ..subject = 'Seu amigo secreto!!'
      ..html = "<h2>Anote o nome do seu amigo secreto:</h2><h1> $_amigo</h1>\n<p> E lembre-se de não contar para os outros. 😉</p>";

    final sendReports2 = await send(equivalentMessage, smtpServer);
    print(sendReports2.first.validationProblems.toString());
  }


}