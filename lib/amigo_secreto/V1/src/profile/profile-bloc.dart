import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:study/amigo_secreto/V1/src/profile/profile-model.dart';
import 'package:study/amigo_secreto/V1/src/shared/service/database.dart';

class ProfileBloc extends BlocBase{

  DatabaseHelper databaseHelper = DatabaseHelper.internal();

  var _nome = BehaviorSubject<String>.seeded("");

  Stream<String> get outNome => _nome.stream;

  Sink<String> get inNome => _nome.sink;

  String get nomeValue => _nome.value;

  void salvaNome(String nome){
    inNome.add(nome);
    print("nome");
  }

  var _email = BehaviorSubject<String>.seeded("");

  Stream<String> get outEmail => _email.stream;

  Sink<String> get inEmail => _email.sink;

  String get emailValue => _email.value;

  void salvaEmail(String email){
    inEmail.add(email);
    print("email");
  }

  void carregaProfile() async{
    Profile profile = await databaseHelper.getProfile();
    if(profile != null) {
      inNome.add(profile.nome);
      inEmail.add(profile.email);
      print(profile.toString());
    }
  }

  void add(){
    Profile value = Profile();
    value.nome = _nome.value;
    value.email = _email.value;

    if(databaseHelper.getProfile() != null) {
      databaseHelper.deletProfile();
    }

    databaseHelper.insertProfile(value);

    print("salva profile");
  }

  @override
  void dispose() {
    _nome.close();
    _email.close();
  }

}

ProfileBloc profileBloc = ProfileBloc();