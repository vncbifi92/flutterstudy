import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:study/amigo_secreto/V1/src/profile/profile-bloc.dart';

class QrProfilePage extends StatefulWidget {
  QrProfilePage({Key key}) : super(key: key);

  @override
  _QrProfilePageState createState() => new _QrProfilePageState();
}

class _QrProfilePageState extends State<QrProfilePage>{

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.fromLTRB(0,0,0,50),
      child: Scaffold(
        backgroundColor: Colors.teal,
          appBar: AppBar(
            title: Text("Qr Profile"),
            backgroundColor: Colors.teal,
            elevation: 0,
          ),
          body: Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)
                ),
                color: Colors.white,
                margin: EdgeInsets.all(8),
                elevation: 0,
                child: QrImage(
                  data: profileBloc.nomeValue + "||" + profileBloc.emailValue,
                  padding: EdgeInsets.all(8),
                  gapless: true,
                ),
              ),
            ),
          )
      ),
    );
  }
}