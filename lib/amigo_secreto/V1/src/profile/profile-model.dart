class Profile{

  String _nome;
  String _email;


  Profile();

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  String get nome => _nome;

  set nome(String value) {
    _nome = value;
  }

  @override
  String toString() {
    return 'Profile{_nome: $_nome, _email: $_email}';
  }

  @override
  bool operator ==(other) {
    Profile p2 = other;
    if(this.email == p2.email && this.nome == p2.nome)
      return true;
    else
      return false;
  }

  @override
  int get hashCode => super.hashCode;

  Profile.fromMap(Map<String, dynamic> map) {
    _nome = map['nome'];
    _email = map['email'];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'nome': this._nome,
      'email': this._email
    };
    return map;
  }

}