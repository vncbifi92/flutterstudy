import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:study/amigo_secreto/V1/src/profile/profile-bloc.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => new _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>{

  MaskedTextController _maskedTextController;
  bool initVal = true;
  final formProfileKey = GlobalKey<FormState>();
  final scaffoldProfileKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // sempre q tu quiser que uma coisa só aconteça uma vez , tu usa o método
    // initState , ele é um método que roda antes do widget aparecer na tela
    // ele vai instanciar o seu controller aqui
    // e vai destruir ele no método dispose , então initState - construção e dispose - destruição
    // é mt importante usar esses caras
    _maskedTextController =  MaskedTextController(
      mask: "(00)00000-0000",
    )..addListener(_textMaskListener);

    super.initState();
  }

  void _textMaskListener(){
    if(_maskedTextController.value.text.length > 1){
      _maskedTextController.updateText(_maskedTextController.value.text);
    }
  }

  @override
  void dispose(){
    // como esse cara é um controller , ele consome recursos , então sempre
    // que tu tiver controllers , feche eles no final , blz ?
    // use o dispose de uma stateful widget pra mandar todos os controllers
    // pra pqp euheuehuehe , assim vc libera recursos
    _maskedTextController.dispose();
    super.dispose();
  }

  _confirmaPerfil(ProfileBloc bloc) {
    final form = formProfileKey.currentState;
    if (form.validate()) {
      form.save();
      bloc.add();
      _showSnackBar("Profile salvo.");
    }
  }


  Widget _cadastroProfile(ProfileBloc bloc){

    return Form(
      key: formProfileKey,
      child: SafeArea(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)
                  ),
                  color: Colors.white,
                  elevation: 0,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: StreamBuilder(
                            stream: bloc.outNome,
                            initialData: bloc.nomeValue,
                            builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                              return TextFormField(
                                  validator: (value){
                                    if(value.isEmpty){
                                      return "Informe o seu nome.";
                                    }
                                  },
                                  initialValue: snapshot.data,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    icon: Icon(Icons.perm_contact_calendar),
                                    labelText: "Nome",
                                  ),
                                  onSaved: (String val) {
                                    bloc.salvaNome(val);
                                  }
                              );
                            }
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: StreamBuilder(
                            initialData: bloc.emailValue,
                            stream: bloc.outEmail,
                            builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                              return TextFormField(
                                  validator: (value){
                                    if(value.isEmpty){
                                      return "Informe o seu email.";
                                    }
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                  initialValue: snapshot.data,
                                  decoration: InputDecoration(
                                    icon: Icon(Icons.email),
                                    labelText: "Email",
                                  ),
                                  onSaved: (String val) {
                                    bloc.salvaEmail(val);
                                  }
                              );
                            }
                        ),
                      ),
                      Divider(
                        color: Colors.transparent,
                      )
                    ],
                  ),
                ),
              )
            ],
          )
      ),
    );
  }

  _showSnackBar(String text){
    scaffoldProfileKey.currentState.showSnackBar(SnackBar(content: Text(text)));
  }

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.fromLTRB(0,0,0,50),
      child: Scaffold(
          key: scaffoldProfileKey,
          appBar: AppBar(
            title: Text("Meu Perfil"),
            backgroundColor: Colors.teal,
            elevation: 0,
            actions: <Widget>[
              IconButton(
                  tooltip: "Profile",
                  icon: Icon(
                    MdiIcons.qrcode,
                  ),
                  onPressed: (){Navigator.of(context).pushNamed('/qrCode');}
              )
            ],
          ),
          body: Container(
            color: Colors.teal,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: SafeArea(
                child: ListView(
                  children: <Widget>[
                    _cadastroProfile(profileBloc)
                  ],
                ),
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.check),
            backgroundColor: Colors.red,
            onPressed: () {
              _confirmaPerfil(profileBloc);
            },
          )
      ),
    );
  }
}