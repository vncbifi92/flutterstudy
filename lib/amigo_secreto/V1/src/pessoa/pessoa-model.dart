class Pessoa{

  int _id;
  int _grupoId;
  String _nome;
  String _email;

  Pessoa(this._nome, this._email, this._grupoId);

  int get grupoId => _grupoId;

  set grupoId(int value){
    _grupoId = value;
  }

  String get nome => _nome;

  set nome(String value) {
    _nome = value;
  }

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  @override
  String toString() {
    return 'Pessoa{_id: $_id, _grupoId: $_grupoId, _nome: $_nome, _email: $_email}';
  }

  @override
  bool operator ==(other) {
    if (_id == other._id && _grupoId == other._grupoId && _nome == other._nome && _email == other._email) {
      return true;
    }else{
      return false;
    }
  }

  @override
  int get hashCode => super.hashCode;

  Pessoa.fromMap(Map<String, dynamic> map) {
    _id = map['id'];
    _grupoId = map['grupoId'];
    _nome = map['nome'];
    _email = map['email'];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'grupoId': this._grupoId,
      'nome': this._nome,
      'email': this._email,
    };
    if (_id != null) {
      map["id"] = _id;
    }
    return map;
  }

}