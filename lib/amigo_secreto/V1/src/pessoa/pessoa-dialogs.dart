import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:study/amigo_secreto/V1/src/pessoa/pessoa-bloc.dart';

class Dialogs{

  cadastroPessoa(BuildContext context, int id) {
    final formKey = GlobalKey<FormState>();

    _salvaPessoa(PessoaBloc bloc) {
      bloc.inGrupoId.add(id);
      final form = formKey.currentState;
      if (form.validate()) {
        form.save();
        bloc.add();
        Navigator.of(context).pop();
      }
    }

    _salvaPessoaQR(PessoaBloc bloc) {
      bloc.inGrupoId.add(id);
      bloc.add();
      Navigator.of(context).pop();
    }

    Widget _okButton(PessoaBloc bloc) {
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Confirmar",
          style: TextStyle(
              color: Colors.orange
          ),
        ),
        onPressed: () {
          _salvaPessoa(bloc);
        },
      );
    }

    Widget _cancelButton() {
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Cancelar",
          style: TextStyle(
              color: Colors.orange
          ),
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      );
    }

    Future _scanQR(PessoaBloc bloc) async {
      String qrResult = await BarcodeScanner.scan();
      print(qrResult);

      List<String> campos = qrResult.split("||");

      if(campos != null && campos.isNotEmpty){
        pessoaBloc.salvaNome(campos[0]);
        pessoaBloc.salvaEmail(campos[1]);

        _salvaPessoaQR(bloc);

      }
    }

    Widget _lerQrCode(PessoaBloc bloc) {
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Ler QrCode",
          style: TextStyle(
              color: Colors.orange
          ),
        ),
        onPressed: () {
          _scanQR(bloc);
        },
      );
    }

    Widget _pessoaForm(PessoaBloc bloc) {
      bloc.inNome.add("");
      bloc.inEmail.add("");
      return Form(
          key: formKey,
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (String val) {
                      final isDigitsOnly = int.tryParse(val);
                      return isDigitsOnly == null
                          ? null
                          : 'O nome deve conter letras.';
                    },
                    onSaved: (String val) {
                      bloc.salvaNome(val);
                    },
                    decoration: InputDecoration(
                        labelText: "Nome",
                        border: OutlineInputBorder()),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    validator: (String val) {
                      final isDigitsOnly = int.tryParse(val);
                      return isDigitsOnly == null
                          ? null
                          : 'O email deve conter letras.';
                    },
                    onSaved: (String val) {
                      bloc.salvaEmail(val);
                    },
                    decoration: InputDecoration(
                        labelText: "Email",
                        border: OutlineInputBorder()),
                  ),
                )
              ],
            ),
          )
      );
    }

    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            content: _pessoaForm(pessoaBloc),
            actions: <Widget>[
              _lerQrCode(pessoaBloc),
              _cancelButton(),
              _okButton(pessoaBloc)
            ],
          );
        }
    );
  }



  sorteioFinalizadoDialog(BuildContext context){

    Widget _okButton(){
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Ok",
          style: TextStyle(
              color: Colors.green
          ),
        ),
        onPressed: (){Navigator.of(context).pop();},
      );
    }

    return  showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text("Sorteio realizado com sucesso."),
            content: Text("Cada participante recebeu um e-mail com o nome de seu amigo secreto."),
            actions: <Widget>[
              _okButton()
            ],
          );
        }
    );
  }

}