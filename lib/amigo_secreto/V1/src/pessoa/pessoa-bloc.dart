import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:study/amigo_secreto/V1/src/pessoa/pessoa-model.dart';
import 'package:study/amigo_secreto/V1/src/shared/service/database.dart';

class PessoaBloc extends BlocBase{

  DatabaseHelper databaseHelper = DatabaseHelper.internal();

  var _nome = BehaviorSubject<String>.seeded("");

  Stream<String> get outNome => _nome.stream;

  Sink<String> get inNome => _nome.sink;

  String get nomeValue => _nome.value;

  void salvaNome(String nome){
    inNome.add(nome);
    print("nome");
  }

  var _email = BehaviorSubject<String>.seeded("");

  Stream<String> get outEmail => _email.stream;

  Sink<String> get inEmail => _email.sink;

  String get emailValue => _email.value;

  void salvaEmail(String email){
    inEmail.add(email);
    print("email");
  }

  var _grupoId = BehaviorSubject<int>.seeded(0);

  Stream<int> get outGrupoId => _grupoId.stream;

  Sink<int> get inGrupoId  => _grupoId.sink;

  int get grupoIdValue => _grupoId.value;

  void salvaGrupoId(int grupoId){
    inGrupoId.add(grupoId);
    print("grupoId");
  }

  var _pessoa = BehaviorSubject<Pessoa>.seeded(Pessoa("", "", 0));

  Stream<Pessoa> get outPessoa => _pessoa.stream;

  Sink<Pessoa> get inPessoa => _pessoa.sink;

  Pessoa get pessoaValue => _pessoa.value;

  void salvaPessoa(Pessoa pessoa){
    inPessoa.add(pessoa);
    print("pessoa");
  }

  var _listPessoa = BehaviorSubject<List<Pessoa>>.seeded(List<Pessoa>());

  Stream<List<Pessoa>> get outList => _listPessoa.stream;

  Sink<List<Pessoa>> get inList => _listPessoa.sink;

  List<Pessoa> get listPessoaValue => _listPessoa.value;

  @override
  void dispose() {
    _grupoId.close();
    _nome.close();
    _pessoa.close();
    _email.close();
    _listPessoa.close();
  }

  void carregaLista(int idGrupo) async{
    List<Pessoa> pessoas = await databaseHelper.getItens(idGrupo);
    pessoas.sort((a, b) {
      return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
    });
    _listPessoa.value = pessoas;
  }

  void add()async{
    Pessoa value = Pessoa(_nome.value, _email.value, _grupoId.value);
    Pessoa pessoaAux = await databaseHelper.getPessoa(_nome.value, _grupoId.value);
    if(pessoaAux == null) {
      databaseHelper.insertPessoa(value);
      _listPessoa.value.add(value);
      _listPessoa.value.sort((a, b) {
        return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
      });
      inList.add(_listPessoa.value);
      print("pessoa adicionado");
    }else{
      print("pessoa duplicado");
    }
  }

  void edit() async{
    Pessoa pessoaAux = await databaseHelper.getPessoa(_nome.value, _grupoId.value);
    if(pessoaAux == null || pessoaAux.id == _pessoa.value.id) {
      Pessoa value = _pessoa.value;
      value.nome = _nome.value;
      value.grupoId = _grupoId.value;
      value.email = _email.value;
      value.id = _pessoa.value.id;
      databaseHelper.updatePessoa(value);
      _listPessoa.value.sort((a, b) {
        return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
      });
      inList.add(_listPessoa.value);
      print("pessoa editado");
    }else{
      print("pessoa duplicado");
    }
  }

  void remove(){
    databaseHelper.deletePessoa(_pessoa.value.id);
    _listPessoa.value.remove(_pessoa.value);
    inList.add(_listPessoa.value);
    print("pessoa removido");
  }
}

PessoaBloc pessoaBloc = PessoaBloc();