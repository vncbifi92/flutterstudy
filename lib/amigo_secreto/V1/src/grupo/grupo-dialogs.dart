import 'package:flutter/material.dart';
import 'package:study/amigo_secreto/V1/src/grupo/grupo-bloc.dart';

class Dialogs{

  cadastroGrupo(BuildContext context) {

    final formKey = GlobalKey<FormState>();

    _salvaGrupo(GrupoBloc bloc) {
      final form = formKey.currentState;
      if (form.validate()) {
        form.save();
        bloc.add();
        Navigator.of(context).pop();
      }
    }

    Widget _okButton(GrupoBloc bloc){
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Confirmar",
          style: TextStyle(
              color: Colors.green
          ),
        ),
        onPressed: (){_salvaGrupo(bloc);},
      );
    }

    Widget _cancelButton(){
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Cancelar",
          style: TextStyle(
              color: Colors.red
          ),
        ),
        onPressed: (){Navigator.pop(context);},
      );
    }

    Widget _grupoForm(GrupoBloc bloc){
      return Form(
          key: formKey,
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: TextFormField(
              validator: (String val){
                final isDigitsOnly = int.tryParse(val);

                final isNull = val == null || val.length <= 0;

                if(isDigitsOnly != null){
                  return 'O nome deve conter letras.';
                }

                if(isNull){
                  return 'O grupo deve ter um nome.';
                }

                return null;
              },
              onSaved: (String val){bloc.salvaNome(val);},
              decoration: InputDecoration(
                  labelText: "Nome",
                  border: OutlineInputBorder()),
            ),
          )
      );
    }

    return  showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text("Iniciar um novo grupo"),
            content: _grupoForm(grupoBloc),
            actions: <Widget>[
              _cancelButton(),
              _okButton(grupoBloc)
            ],
          );
        }
    );
  }
}