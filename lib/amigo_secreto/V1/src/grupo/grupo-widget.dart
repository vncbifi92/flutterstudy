import 'package:flutter/material.dart';
import 'package:study/amigo_secreto/V1/src/grupo/grupo-bloc.dart';
import 'package:study/amigo_secreto/V1/src/grupo/grupo-dialogs.dart';
import 'package:study/amigo_secreto/V1/src/grupo/grupo-model.dart';

class GrupoPage extends StatefulWidget {
  GrupoPage({Key key}) : super(key: key);


  @override
  _GrupoPageState createState() => new _GrupoPageState();
}


class _GrupoPageState extends State<GrupoPage>{


  _removeGrupo(GrupoBloc bloc, Grupo grupo) {
    bloc.salvaGrupo(grupo);
    bloc.remove();
  }


  _editarGrupo(GrupoBloc bloc, Grupo grupo) {
    bloc.salvaNome(grupo.nome);
    bloc.salvaGrupo(grupo);
    Navigator.of(context).pushNamed("/edit_grupo");
  }


  _novoGrupo(){
    Dialogs dialogs = new Dialogs();
    dialogs.cadastroGrupo(context);
  }


  Widget _listaGrupos(GrupoBloc bloc){
    return StreamBuilder(
        stream: bloc.outList,
        builder: (BuildContext context, AsyncSnapshot<List<Grupo>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                itemBuilder: (context, index) {
                  if (index < snapshot.data.length) {
                    return Column(
                      children: <Widget>[
                        GestureDetector(
                          child: Card(
                            child: ListTile(
                              title: Text(
                                  snapshot.data.elementAt(index).nome
                              ),
                              onTap: () => _editarGrupo(bloc, snapshot.data.elementAt(index)),
                            ),
                          ),
                          onLongPress: () =>
                              _removeGrupo(bloc, snapshot.data.elementAt(index)),
                        ),
                      ],
                    );
                  }
                }
            );
          }else{
            return Container();
          }
        }
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.fromLTRB(0,0,0,50),
      child: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            IconButton(
                tooltip: "Profile",
                icon: Icon(
                  Icons.person,
                ),
                onPressed: (){Navigator.of(context).pushNamed('/profile');}
            )
          ],
          backgroundColor: Colors.teal,
          elevation: 0,
          title: Text(
            "Amis",
            style: TextStyle(
                color: Colors.white
            ),
          )
        ),
        body: Container(
            color: Colors.teal,
            padding: EdgeInsets.all(10.0),
            child: _listaGrupos(grupoBloc)
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.red,
          child: Icon(Icons.add),
          onPressed: _novoGrupo
        ),
      ),
    );
  }
}