import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:study/amigo_secreto/V1/src/shared/service/database.dart';
import 'package:study/amigo_secreto/V1/src/grupo/grupo-model.dart';

class GrupoBloc extends BlocBase{

  DatabaseHelper databaseHelper = DatabaseHelper.internal();

  var _nome = BehaviorSubject<String>.seeded("");

  Stream<String> get outNome => _nome.stream;

  Sink<String> get inNome => _nome.sink;

  String get nomeValue => _nome.value;

  void salvaNome(String nome){
    inNome.add(nome);
    print("nome");
  }

  var _grupo = BehaviorSubject<Grupo>.seeded(Grupo(""));

  Stream<Grupo> get outGrupo => _grupo.stream;

  Sink<Grupo> get inGrupo => _grupo.sink;

  Grupo get grupoValue => _grupo.value;

  void salvaGrupo(Grupo grupo){
    inGrupo.add(grupo);
    print("grupo");
  }

  var _listGrupo = BehaviorSubject<List<Grupo>>.seeded(List<Grupo>());

  Stream<List<Grupo>> get outList => _listGrupo.stream;

  Sink<List<Grupo>> get inList => _listGrupo.sink;

  List<Grupo> get listGrupoValue => _listGrupo.value;

  @override
  void dispose() {
    _nome.close();
    _grupo.close();
    _listGrupo.close();
  }

  void carregaLista() async{
    List<Grupo> grupos = await databaseHelper.getGrupos();
    grupos.sort((a, b) {
      return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
    });
    _listGrupo.value = grupos;
  }

  void add() async {
    Grupo grupoAux = await databaseHelper.getGrupo(_nome.value);
    if(grupoAux == null) {
      Grupo value = Grupo(_nome.value);
      await databaseHelper.insertGrupo(value);
      _listGrupo.value.add(value);
      _listGrupo.value.sort((a, b) {
        return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
      });
      inList.add(_listGrupo.value);
      print("salva lista");
    }else{

    }
  }

  void edit()async{
    Grupo grupoAux = await databaseHelper.getGrupo(_nome.value);
    if(grupoAux == null) {
      Grupo value = _grupo.value;
      value.nome = _nome.value;
      databaseHelper.updateGrupo(value);
      _listGrupo.value.sort((a, b) {
        return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
      });
      inList.add(_listGrupo.value);
    }else{

    }
  }

  void remove(){
    databaseHelper.deleteGrupo(_grupo.value.id);
    _listGrupo.value.remove(_grupo.value);
    inList.add(_listGrupo.value);
    print("remove lista");
  }
}

GrupoBloc grupoBloc = GrupoBloc();