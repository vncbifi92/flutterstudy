import 'package:flutter/material.dart';
import 'package:study/amigo_secreto/V1/src/grupo/grupo-bloc.dart';
import 'package:study/amigo_secreto/V1/src/mensagem/mensagem-util.dart';
import 'package:study/amigo_secreto/V1/src/pessoa/pessoa-bloc.dart';
import 'package:study/amigo_secreto/V1/src/pessoa/pessoa-dialogs.dart';
import 'package:study/amigo_secreto/V1/src/pessoa/pessoa-model.dart';

class EditGrupoPage extends StatefulWidget {
  EditGrupoPage({Key key}) : super(key: key);

  @override
  _EditGrupoPageState createState() => new _EditGrupoPageState();
}

class _EditGrupoPageState extends State<EditGrupoPage>{
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  _cadastroPessoa(int id){
    Dialogs dialogs = new Dialogs();
    dialogs.cadastroPessoa(context, id);
  }

  _removePessoa(PessoaBloc bloc, Pessoa pessoa){
    bloc.salvaPessoa(pessoa);
    bloc.remove();
  }

  Widget _listaPessoas(PessoaBloc bloc){
    return StreamBuilder(
        stream: bloc.outList,
        builder: (BuildContext context, AsyncSnapshot<List<Pessoa>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                itemBuilder: (context, index) {
                  if (index < snapshot.data.length) {
                    return Column(
                      children: <Widget>[
                        GestureDetector(
                          child: ListTile(
                            title: Text(
                                snapshot.data.elementAt(index).nome
                            ),
                            subtitle: Text(
                                snapshot.data.elementAt(index).email.toString()
                            ),
                          ),
                          onLongPress: () =>
                              _removePessoa(bloc, snapshot.data.elementAt(index)),
                        ),
                        Divider(),
                      ],
                    );
                  }
                }
            );
          }else{
            return Divider();
          }
        }
    );
  }

  _sortear(PessoaBloc bloc){
    MensagemUtil mensagemUtil = MensagemUtil();
    mensagemUtil.sortearAmigos(bloc.listPessoaValue);
    Dialogs dialogs = new Dialogs();
    dialogs.sorteioFinalizadoDialog(context);
  }

  @override
  Widget build(BuildContext context) {

    pessoaBloc.carregaLista(grupoBloc.grupoValue.id);

    return Padding(
      padding: const EdgeInsets.fromLTRB(0,0,0,50),
      child: Scaffold(
        appBar: AppBar(
            iconTheme: IconThemeData(
                color: Colors.white
            ),
            actions: <Widget>[
              IconButton(
                  tooltip: "Sortear",
                  icon: Icon(
                    Icons.check,
                  ),
                  onPressed: (){
                    _sortear(pessoaBloc);
                  }
              )
            ],
            backgroundColor: Colors.teal,
            elevation: 0,
            title: Text(
              grupoBloc.nomeValue,
              style: TextStyle(
                  color: Colors.white
              ),
            )
        ),
        body: Container(
          color: Colors.teal,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0)
              ),
              color: Colors.white,
              child: SafeArea(
                  child: _listaPessoas(pessoaBloc)
              )
            ),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          color: Colors.teal,
          child:  MaterialButton(
            onPressed:(){
              _cadastroPessoa(grupoBloc.grupoValue.id);
            } ,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(height: 8), //Espaço em branco
                Icon(Icons.add, color: Colors.white,),
                Text("Adicionar pessoa", style: TextStyle(fontSize: 20, color: Colors.white),),
                SizedBox(height: 8), //Espaço em branco
              ],
            ),
          ),
        ),
      ),
    );
  }
}