import 'package:flutter/material.dart';
import 'package:study/amigo_secreto/V1/src/grupo/grupo-bloc.dart';
import 'package:study/amigo_secreto/V1/src/grupo/grupo-widget.dart';
import 'package:study/amigo_secreto/V1/src/profile/profile-bloc.dart';
import 'package:study/amigo_secreto/V1/src/shared/routes.dart';

class AppWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    profileBloc.carregaProfile();
    grupoBloc.carregaLista();

    return MaterialApp(
      routes: routes,
      debugShowCheckedModeBanner: false,
      home:GrupoPage()
    );
  }
}