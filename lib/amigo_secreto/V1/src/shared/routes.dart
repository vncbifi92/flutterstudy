import 'package:flutter/material.dart';
import 'package:study/amigo_secreto/V1/src/profile/profile-widget.dart';
import 'package:study/amigo_secreto/V1/src/profile/qrProfile/qrProfile-widget.dart';
import 'package:study/amigo_secreto/V1/src/grupo/grupo-widget.dart';
import 'package:study/amigo_secreto/V1/src/grupo/grupo-edit-widget.dart';

final routes = {
  '/profile': (BuildContext context) => ProfilePage(),
  '/qrCode': (BuildContext context) => QrProfilePage(),
  '/viagem': (BuildContext context) => GrupoPage(),
  '/edit_grupo': (BuildContext context) => EditGrupoPage(),
};