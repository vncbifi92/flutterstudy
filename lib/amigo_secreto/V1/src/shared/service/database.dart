import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:study/amigo_secreto/V1/src/pessoa/pessoa-model.dart';
import 'dart:io' as io;
import 'package:study/amigo_secreto/V1/src/profile/profile-model.dart';
import 'package:study/amigo_secreto/V1/src/grupo/grupo-model.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if(_db != null){
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "main.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate, onUpgrade: _onUpgrade, onDowngrade: _onDowngrade);
    return theDb;
  }

  void _onDowngrade(Database db, int oldVersion, int newVersion) {
    db.execute("DROP TABLE IF EXISTS Profile");
    db.execute("DROP TABLE IF EXISTS Grupo");
    db.execute("DROP TABLE IF EXISTS Pessoa");
    _onCreate(db, newVersion);
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) {
    db.execute("DROP TABLE IF EXISTS Profile");
    db.execute("DROP TABLE IF EXISTS Grupo");
    db.execute("DROP TABLE IF EXISTS Pessoa");
    _onCreate(db, newVersion);
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE Profile( nome TEXT"
            ", email TEXT)");

    await db.execute(
        "CREATE TABLE Grupo( id INTEGER PRIMARY KEY AUTOINCREMENT"
            ", nome TEXT )");

    await db.execute(
        "CREATE TABLE Pessoa( id INTEGER PRIMARY KEY AUTOINCREMENT"
            ", grupoId INTEGER"
            ", nome TEXT"
            ", email TEXT )");
  }

  Future closeDb() async {
    var dbClient = await db;
    dbClient.close();
  }

  //Profile>>>>>>>>
  Future<Profile> insertProfile(Profile profile) async {
    var dbClient = await db;
    await dbClient.insert("Profile", profile.toMap());
    return profile;
  }

  Future<int> deletProfile() async {
    var dbClient = await db;
    return await dbClient.delete("Profile");
  }

  Future<int> updateProfile(Profile profile) async {
    var dbClient = await db;
    return await dbClient.update("Profile", profile.toMap());
  }

  Future<Profile> getProfile() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Profile",
        columns: ["nome", "email"]);
    if (maps.length > 0) {
      return new Profile.fromMap(maps.first);
    }
    return null;
  }
  //<<<<<<<Profile

//Pessoa>>>>>>>>
  Future<List<Pessoa>> getItens(int idGrupo) async {
    List<Pessoa> pessoas = new List<Pessoa>();
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Pessoa", where: "grupoId = ?", whereArgs: [idGrupo]);
    if (maps.length > 0) {
      for (Map pessoa in maps) {
        pessoas.add(Pessoa.fromMap(pessoa));
      }
    }
    return pessoas;
  }

  Future<Pessoa> insertPessoa(Pessoa pessoa) async {
    var dbClient = await db;
    getPessoa(pessoa.nome, pessoa.grupoId);
    pessoa.id = await dbClient.insert("Pessoa", pessoa.toMap());
    return pessoa;
  }

  Future<int> deletePessoa(int id) async {
    var dbClient = await db;
    return await dbClient.delete("Pessoa", where: "id = ?", whereArgs: [id]);
  }

  Future<int> deletePessoaDeGrupo(int grupoId) async {
    var dbClient = await db;
    return await dbClient.delete("Pessoa", where: "grupoId = ?", whereArgs: [grupoId]);
  }

  Future<int> updatePessoa(Pessoa pessoa) async {
    var dbClient = await db;
    int qtd = await dbClient.update("Pessoa", pessoa.toMap(),
        where: "id = ?", whereArgs: [pessoa.id]);
    return qtd;
  }

  Future<Pessoa> getPessoa(String nome, int grupoId) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Pessoa",
        columns: ["id", "grupoId", "nome", "email"],
        where: "nome = ? and grupoId = ?",
        whereArgs: [nome, grupoId]);
    if (maps.length > 0) {
      return new Pessoa.fromMap(maps.first);
    }
    return null;
  }

  //<<<<<<<Pessoa

  //Grupo>>>>>>>>
  Future<Grupo> insertGrupo(Grupo grupo) async {
    var dbClient = await db;
    grupo.id = await dbClient.insert("Grupo", grupo.toMap());
    return grupo;
  }

  Future<int> deleteGrupo(int id) async {
    var dbClient = await db;
    await deletePessoaDeGrupo(id);
    return await dbClient.delete("Grupo", where: "id = ?", whereArgs: [id]);
  }

  Future<int> updateGrupo(Grupo grupo) async {
    var dbClient = await db;
    int qtd = await dbClient.update("Grupo", grupo.toMap());
    return qtd;
  }

  Future<Grupo> getGrupo(String nome) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Grupo",
        columns: ["id", "nome"],
        where: "nome = ?",
        whereArgs: [nome]
    );
    if (maps.length > 0) {
      return new Grupo.fromMap(maps.first);
    }
    return null;
  }

  Future<List<Grupo>> getGrupos() async {
    List<Grupo> grupos = new List<Grupo>();
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Grupo");
    if (maps.length > 0) {
      for (Map grupo in maps) {
        grupos.add(Grupo.fromMap(grupo));
      }
    }
    return grupos;
  }
//<<<<<<<Grupo

}