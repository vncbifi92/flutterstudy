import 'package:flutter/material.dart';
import 'index-model.dart';

class IndexPage extends StatefulWidget {
  IndexPage({Key key}) : super(key: key);

  @override
  _IndexPageState createState() => new _IndexPageState();
}

class _IndexPageState extends State<IndexPage>{

  Widget _led(Led led){
    return StreamBuilder<bool>(
      stream: led.indexBloc.outAnimar,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Card(
            child: Container(
              height: 30,
              width: 20,
            ),
            elevation: 8,
            margin: EdgeInsets.all(10),
            color: snapshot.data ? led.color : Colors.teal,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(180)
            ),
          );
        }else{
          return Card();
        }
      }
    );
  }

  @override
  Widget build(BuildContext context) {

    Led led1 = Led(Colors.cyanAccent, 1);
    Led led2 = Led(Colors.amberAccent, 2);
    Led led3 = Led(Colors.blueAccent, 1);
    Led led4 = Led(Colors.deepOrangeAccent, 1);
    Led led5 = Led(Colors.deepPurpleAccent, 2);
    Led led6 = Led(Colors.greenAccent, 3);

    return  Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        elevation: 0,
        title: Text(
          "Index",
          style: TextStyle(
              color: Colors.white
          ),
        )
      ),
      body: Container(
        color: Colors.teal,
        padding: EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(child: _led(led1)),
            Center(child: _led(led2)),
            Center(child: _led(led3)),
            Center(child: _led(led4)),
            Center(child: _led(led5)),
            Center(child: _led(led6)),
          ],
        )
      ),
    );
  }
}