import 'dart:async';
import 'package:flutter/material.dart';
import 'package:study/animacao/V1/src/index/index-bloc.dart';

class Led{

  Color _color = Colors.black;
  Timer _timer;
  IndexBloc _indexBloc = IndexBloc();

  _ligaDesliga(Timer timer){
    _indexBloc.switchAnimar();
  }

  Led(Color color, int timerSec){
    this._timer = Timer.periodic(Duration(seconds: timerSec), _ligaDesliga);
    this._color = color;
  }

  IndexBloc get indexBloc => _indexBloc;

  set indexBloc(IndexBloc value){
    _indexBloc = value;
  }

  Timer get timer => _timer;

  set timer(Timer value) {
    _timer = value;
  }

  Color get color => _color;

  set color(Color value) {
    _color = value;
  }


}