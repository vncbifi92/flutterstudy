import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:study/animacao/V1/src/shared/service/database.dart';

class NotasBloc extends BlocBase{

  var _animar = BehaviorSubject<bool>.seeded(false);

  Stream<bool> get outAnimar => _animar.stream;

  Sink<bool> get inAnimar => _animar.sink;

  bool get animarValue => _animar.value;

  void salvaAnimar(bool animar){
    inAnimar.add(animar);
    print("animar notas $animar");
  }

  void switchAnimar(){
    salvaAnimar(!animarValue);
  }

  void addNota(int usuarioId){

    MyDatabase.instance.addNota(
        Nota(
            data: DateTime.now(),
            nota: "teste de nota",
            titulo: "teste de titulo",
            usuarioId: usuarioId
        )
    );
  }


  void removeNota(Nota nota){
    MyDatabase.instance.removeNota(nota);
  }



  @override
  void dispose() {
    _animar.close();
  }

}

NotasBloc notasBloc = NotasBloc();