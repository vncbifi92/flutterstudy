import 'package:flutter/material.dart';
import 'package:study/animacao/V1/src/notas/notas-bloc.dart';
import 'package:study/animacao/V1/src/shared/service/database.dart';

class NotasPage extends StatefulWidget {
  NotasPage({Key key, this.usuarioId}) : super(key: key);

  final int usuarioId;

  @override
  _NotasPageState createState() => new _NotasPageState();
}

class _NotasPageState extends State<NotasPage>{

  _removeNota(Nota nota) async{
    notasBloc.removeNota(nota);
  }

  _novaNota() async{
    notasBloc.addNota(widget.usuarioId);
  }

  Widget _listaNotas(){
    return StreamBuilder(
        stream: MyDatabase.instance.getNotas(widget.usuarioId),
        builder: (BuildContext context, AsyncSnapshot<List<Nota>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                itemBuilder: (context, index) {
                  if (index < snapshot.data.length) {
                    return Column(
                      children: <Widget>[
                        GestureDetector(
                          child: ListTile(
                            title: Text(
                                snapshot.data.elementAt(index).titulo
                            ),
                            subtitle: Text(
                                snapshot.data.elementAt(index).nota + snapshot.data.elementAt(index).id.toString()
                            ),
                          ),
                          onLongPress: () => _removeNota(snapshot.data.elementAt(index)),
                        ),
                        Divider(),
                      ],
                    );
                  }
                }
            );
          }else{
            return Divider();
          }
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        elevation: 0,
        title: Text(
            "Notas",
          style: TextStyle(
              color: Colors.white
          ),
        )
      ),
      body: _listaNotas(),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: (){_novaNota();}
      ),
    );
  }
}