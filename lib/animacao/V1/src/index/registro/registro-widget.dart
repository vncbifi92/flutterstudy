import 'package:flutter/material.dart';
import 'package:study/animacao/V1/src/index/registro/registro-bloc.dart';
import 'package:study/animacao/V1/src/index/index-bloc.dart' as indexBloc;
import 'package:study/animacao/V1/src/notas/notas-widget.dart';

class Registro {

  final formKey = GlobalKey<FormState>();

  String _nome, _senha, _email;

  _registra(BuildContext context)async{

    if(formKey.currentState.validate()) {
      formKey.currentState?.save();

      int usuarioId = await registroBloc.registro(_nome, _senha, _email);

      Navigator.push(
          context,
          MaterialPageRoute(
              settings: const RouteSettings(name: '/notas'),
              builder: (context) => NotasPage(
                usuarioId: usuarioId,
              )
          )
      );
    }
  }

  _switchAnimarRegistro(){
    registroBloc.switchAnimar();
    indexBloc.indexBloc.switchAnimar();
  }

  Widget _formRegistro(BuildContext context){
    return SingleChildScrollView(
      child: Center(
        child: Form(
            key: formKey,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Title(color: Colors.white, child: Text("Registro", style: TextStyle(color: Colors.white))),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: StreamBuilder<Object>(
                        stream: registroBloc.outAnimar,
                        builder: (context, snapshot) {
                          return TextFormField(
                              validator: (value){
                                if(value.length < 3){
                                  return "Este campo deve conter no minimo 3 caracteres.";
                                }
                              },
                            onSaved: (val){
                              _nome = val;
                            },
                            cursorColor: Colors.white,
                            enabled: snapshot.hasData ? snapshot.data : true,
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                labelText: "Nome",
                                labelStyle: TextStyle(color: Colors.white),
                                border: UnderlineInputBorder()),
                          );
                        }
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: StreamBuilder<Object>(
                        stream: registroBloc.outAnimar,
                        builder: (context, snapshot) {
                          return TextFormField(
                            validator: (value){
                              if(value.length < 3){
                                return "Este campo deve conter no minimo 3 caracteres.";
                              }
                            },
                            onSaved: (val){
                              _email = val;
                            },
                            keyboardType: TextInputType.emailAddress,
                            cursorColor: Colors.white,
                            enabled: snapshot.hasData ? snapshot.data : true,
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                labelText: "Email",
                                labelStyle: TextStyle(color: Colors.white),
                                border: UnderlineInputBorder()),
                          );
                        }
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: StreamBuilder<Object>(
                        stream: registroBloc.outAnimar,
                        builder: (context, snapshot) {
                          return TextFormField(
                            validator: (value){
                              if(value.length < 3){
                                return "Este campo deve conter no minimo 3 caracteres.";
                              }
                            },
                            onSaved: (val){
                              _senha = val;
                            },
                            cursorColor: Colors.white,
                            obscureText: true,
                            enabled: snapshot.hasData ? snapshot.data : true,
                            decoration: InputDecoration(
                                labelText: "Senha",
                                labelStyle: TextStyle(color: Colors.white),
                                border: UnderlineInputBorder()),
                          );
                        }
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: MaterialButton(
                            child: Text("Ok"),
                            color: Colors.tealAccent,
                            onPressed: (){_registra(context);}
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: MaterialButton(
                            child: Text("Voltar"),
                            color: Colors.tealAccent,
                            onPressed: _switchAnimarRegistro
                        ),
                      )
                    ],
                  ),
                ]
            )
        ),
      ),
    );
  }

  Widget _botaoRegistro(){
    return Center(
      child: Text(
        "Registrar",
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  Widget animaRegistro(){
    return StreamBuilder<Object>(
        stream: registroBloc.outAnimar,
        builder: (context, snapshot) {
          if(snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: GestureDetector(
                    onTap: snapshot.data ? null : _switchAnimarRegistro,
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 500),
                      height: snapshot.data ? 400 : 50,
                      decoration: BoxDecoration(
                          color: snapshot.data ? Colors.teal : Colors.green,
                          borderRadius: BorderRadius.circular(20)
                      ),
                      child: AnimatedCrossFade(
                          firstChild: _formRegistro(context),
                          secondChild: _botaoRegistro(),
                          crossFadeState: snapshot.data
                              ? CrossFadeState.showFirst
                              : CrossFadeState.showSecond,
                          duration: Duration(milliseconds: 500)
                      ),
                    )
                ),
              ),
            );
          }else{
            return Container();
          }
        }
    );
  }

}

Registro widgetRegistro = Registro();