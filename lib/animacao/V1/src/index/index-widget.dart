import 'package:flutter/material.dart';
import 'package:study/animacao/V1/src/index/login/login-bloc.dart' as loginBloc;
import 'package:study/animacao/V1/src/index/registro/registro-bloc.dart' as registroBloc;
import 'package:study/animacao/V1/src/index/index-bloc.dart' as indexBloc;
import 'package:study/animacao/V1/src/index/login/login-widget.dart';
import 'package:study/animacao/V1/src/index/registro/registro-widget.dart';

class IndexPage extends StatefulWidget {
  IndexPage({Key key}) : super(key: key);

  @override
  _IndexPageState createState() => new _IndexPageState();
}

class _IndexPageState extends State<IndexPage> with TickerProviderStateMixin{

  Widget _logo(){

    return StreamBuilder<Object>(
      stream: indexBloc.indexBloc.outAnimar,
      builder: (context, snapshot) {
        if(snapshot.hasData) {

          return AnimatedSize(
            curve: Curves.linear,
              duration: Duration(milliseconds: 1000),
              vsync: this,
              child: Icon(
                Icons.android,
                size: snapshot.data ? 0 : 200,
                color: Colors.white
              )
          );

        }else{
          return Icon(Icons.android, size: 200, color: Colors.white);
        }
      }
    );
  }
  Widget _index(){
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          StreamBuilder(
            stream: registroBloc.registroBloc.outAnimar,
            builder: (context, snapshot) {
              if(snapshot.hasData) {
                return snapshot.data ? Center() : widgetLogin.animaLogin();
              }else{
                return Container();
              }
            }
          ),
          StreamBuilder(
            stream: loginBloc.loginBloc.outAnimar,
            builder: (context, snapshot) {
              if(snapshot.hasData) {
                return snapshot.data ? Center() : widgetRegistro.animaRegistro();
              }else{
                return Container();
              }
            }
          ),
        ],
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        elevation: 0,
        title: Text(
          "Index",
          style: TextStyle(
              color: Colors.white
          ),
        )
      ),
      body: Container(
        color: Colors.teal,
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Center(child: _logo()),
            Expanded(child: _index()),
          ],
        )
      ),
    );
  }
}