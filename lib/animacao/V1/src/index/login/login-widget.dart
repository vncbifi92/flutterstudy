import 'package:flutter/material.dart';
import 'package:study/animacao/V1/src/index/login/login-bloc.dart';
import 'package:study/animacao/V1/src/index/index-bloc.dart' as indexBloc;
import 'package:study/animacao/V1/src/notas/notas-widget.dart';
import 'package:study/animacao/V1/src/shared/service/database.dart';

class Login {

  final formKey = GlobalKey<FormState>();
  String _nome;
  String _senha;


  _login(BuildContext context)async{
    if(formKey.currentState.validate()) {
      formKey.currentState?.save();
      UsuarioData usuario = await loginBloc.login(_nome, _senha);

      if (usuario != null) {
        Navigator.push(
            context,
            MaterialPageRoute(
                settings: const RouteSettings(name: '/notas'),
                builder: (context) => NotasPage(
                  usuarioId: usuario.id,
                )
            )
        );
      } else {
        showDialog(context: context,
            barrierDismissible: true,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text("Usuario e/ou senha invalido."),
                actions: <Widget>[
                  FlatButton(
                    color: Colors.transparent,
                    child: Text(
                      "Ok",
                      style: TextStyle(
                          color: Colors.orange
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              );
            }
        );
      }
    }
    
  }
  
  _switchAnimarLogin(){
    loginBloc.switchAnimar();
    indexBloc.indexBloc.switchAnimar();
  }

  Widget _formLogin(BuildContext context){
    return SingleChildScrollView(
      child: Center(
        child: Form(
            key: formKey,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Title(color: Colors.white, child: Text("Login", style: TextStyle(color: Colors.white))),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: StreamBuilder<Object>(
                        stream: loginBloc.outAnimar,
                        builder: (context, snapshot) {
                          return TextFormField(
                            onSaved: (String val){
                              _nome = val;
                            },
                            cursorColor: Colors.white,
                            enabled: snapshot.hasData ? snapshot.data : true,
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                labelText: "Nome",
                                labelStyle: TextStyle(color: Colors.white),
                                border: UnderlineInputBorder()),
                          );
                        }
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: StreamBuilder<Object>(
                        stream: loginBloc.outAnimar,
                        builder: (context, snapshot) {
                          return TextFormField(
                            onSaved: (String val){
                              _senha = val;
                            },
                            cursorColor: Colors.white,
                            obscureText: true,
                            enabled: snapshot.hasData ? snapshot.data : true,
                            decoration: InputDecoration(
                                labelText: "Senha",
                                labelStyle: TextStyle(color: Colors.white),
                                border: UnderlineInputBorder()),
                          );
                        }
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: MaterialButton(
                            child: Text("Ok"),
                            color: Colors.tealAccent,
                            onPressed: (){_login(context);}
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: MaterialButton(
                            child: Text("Voltar"),
                            color: Colors.tealAccent,
                            onPressed: _switchAnimarLogin
                        ),
                      )
                    ],
                  ),
                ]
            )
        ),
      ),
    );
  }

  Widget _botaoLogin(){
    return Center(
      child: Text(
        "Login",
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  Widget animaLogin(){
    return StreamBuilder<Object>(
        stream: loginBloc.outAnimar,
        builder: (context, snapshot) {
          if(snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: GestureDetector(
                    onTap: snapshot.data ? null : _switchAnimarLogin,
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 500),
                      height: snapshot.data ? 400 : 50,
                      decoration: BoxDecoration(
                          color: snapshot.data ? Colors.teal : Colors.blue,
                          borderRadius: BorderRadius.circular(20)
                      ),
                      child: AnimatedCrossFade(
                          firstChild: _formLogin(context),
                          secondChild: _botaoLogin(),
                          crossFadeState: snapshot.data
                              ? CrossFadeState.showFirst
                              : CrossFadeState.showSecond,
                          duration: Duration(milliseconds: 500)
                      ),
                    )
                ),
              ),
            );
          }else{
            return Container();
          }
        }
    );
  }

}

Login widgetLogin = Login();