import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:study/animacao/V1/src/shared/service/database.dart';

class LoginBloc extends BlocBase{

  var _animar = BehaviorSubject<bool>.seeded(false);

  Stream<bool> get outAnimar => _animar.stream;

  Sink<bool> get inAnimar => _animar.sink;

  bool get animarValue => _animar.value;

  void salvaAnimar(bool animar){
    inAnimar.add(animar);
    print("animar login $animar");
  }

  void switchAnimar(){
    salvaAnimar(!animarValue);
  }

  @override
  void dispose() {
    _animar.close();
  }

  Future<UsuarioData> login(String nome, String senha) async{

    List<UsuarioData> usuario  = await MyDatabase.instance.getUsuario(nome, senha);

    if(usuario.isNotEmpty && usuario.first != null){
      return usuario.first;
    }else {
      return null;
    }

  }

}

LoginBloc loginBloc = LoginBloc();