import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';

class IndexBloc extends BlocBase{

  var _animar = BehaviorSubject<bool>.seeded(false);

  Stream<bool> get outAnimar => _animar.stream;

  Sink<bool> get inAnimar => _animar.sink;

  bool get animarValue => _animar.value;

  void salvaAnimar(bool animar){
    inAnimar.add(animar);
    print("animar index $animar");
  }

  void switchAnimar(){
    salvaAnimar(!animarValue);
  }

  @override
  void dispose() {
    _animar.close();
  }

}

IndexBloc indexBloc = IndexBloc();