import 'package:flutter/material.dart';
import 'package:study/animacao/V1/src/shared/routes.dart';

class AppWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      routes: routes,
      debugShowCheckedModeBanner: false,
    );
  }
}