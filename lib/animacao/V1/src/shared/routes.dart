import 'package:flutter/material.dart';
import 'package:study/animacao/V1/src/index/index-widget.dart';
import 'package:study/animacao/V1/src/notas/notas-widget.dart';

final routes = {
  '/': (BuildContext context) => IndexPage(),
  '/notas': (BuildContext context) => NotasPage(),
};