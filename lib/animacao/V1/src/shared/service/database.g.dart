// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

class UsuarioData {
  final int id;
  final String nome;
  final String senha;
  final String email;
  UsuarioData({this.id, this.nome, this.senha, this.email});
  factory UsuarioData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db) {
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return UsuarioData(
      id: intType.mapFromDatabaseResponse(data['id']),
      nome: stringType.mapFromDatabaseResponse(data['nome']),
      senha: stringType.mapFromDatabaseResponse(data['senha']),
      email: stringType.mapFromDatabaseResponse(data['email']),
    );
  }
  factory UsuarioData.fromJson(Map<String, dynamic> json) {
    return UsuarioData(
      id: json['id'] as int,
      nome: json['nome'] as String,
      senha: json['senha'] as String,
      email: json['email'] as String,
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nome': nome,
      'senha': senha,
      'email': email,
    };
  }

  UsuarioData copyWith({int id, String nome, String senha, String email}) =>
      UsuarioData(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        senha: senha ?? this.senha,
        email: email ?? this.email,
      );
  @override
  String toString() {
    return (StringBuffer('UsuarioData(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('senha: $senha, ')
          ..write('email: $email')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      (((id.hashCode) * 31 + nome.hashCode) * 31 + senha.hashCode) * 31 +
      email.hashCode;
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is UsuarioData &&
          other.id == id &&
          other.nome == nome &&
          other.senha == senha &&
          other.email == email);
}

class $UsuarioTable extends Usuario implements TableInfo<Usuario, UsuarioData> {
  final GeneratedDatabase _db;
  $UsuarioTable(this._db);
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id =>
      _id ??= GeneratedIntColumn('id', false, hasAutoIncrement: true);
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??=
      GeneratedTextColumn('nome', false, minTextLength: 3, maxTextLength: 100);
  GeneratedTextColumn _senha;
  @override
  GeneratedTextColumn get senha => _senha ??=
      GeneratedTextColumn('senha', false, minTextLength: 3, maxTextLength: 100);
  GeneratedTextColumn _email;
  @override
  GeneratedTextColumn get email => _email ??=
      GeneratedTextColumn('email', false, minTextLength: 3, maxTextLength: 100);
  @override
  List<GeneratedColumn> get $columns => [id, nome, senha, email];
  @override
  Usuario get asDslTable => this;
  @override
  String get $tableName => 'usuario';
  @override
  bool validateIntegrity(UsuarioData instance, bool isInserting) =>
      id.isAcceptableValue(instance.id, isInserting) &&
      nome.isAcceptableValue(instance.nome, isInserting) &&
      senha.isAcceptableValue(instance.senha, isInserting) &&
      email.isAcceptableValue(instance.email, isInserting);
  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  UsuarioData map(Map<String, dynamic> data) {
    return UsuarioData.fromData(data, _db);
  }

  @override
  Map<String, Variable> entityToSql(UsuarioData d,
      {bool includeNulls = false}) {
    final map = <String, Variable>{};
    if (d.id != null || includeNulls) {
      map['id'] = Variable<int, IntType>(d.id);
    }
    if (d.nome != null || includeNulls) {
      map['nome'] = Variable<String, StringType>(d.nome);
    }
    if (d.senha != null || includeNulls) {
      map['senha'] = Variable<String, StringType>(d.senha);
    }
    if (d.email != null || includeNulls) {
      map['email'] = Variable<String, StringType>(d.email);
    }
    return map;
  }
}

class Nota {
  final int id;
  final int usuarioId;
  final DateTime data;
  final String titulo;
  final String nota;
  Nota({this.id, this.usuarioId, this.data, this.titulo, this.nota});
  factory Nota.fromData(Map<String, dynamic> data, GeneratedDatabase db) {
    final intType = db.typeSystem.forDartType<int>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    final stringType = db.typeSystem.forDartType<String>();
    return Nota(
      id: intType.mapFromDatabaseResponse(data['id']),
      usuarioId: intType.mapFromDatabaseResponse(data['usuario_id']),
      data: dateTimeType.mapFromDatabaseResponse(data['data']),
      titulo: stringType.mapFromDatabaseResponse(data['titulo']),
      nota: stringType.mapFromDatabaseResponse(data['nota']),
    );
  }
  factory Nota.fromJson(Map<String, dynamic> json) {
    return Nota(
      id: json['id'] as int,
      usuarioId: json['usuarioId'] as int,
      data: json['data'] as DateTime,
      titulo: json['titulo'] as String,
      nota: json['nota'] as String,
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'usuarioId': usuarioId,
      'data': data,
      'titulo': titulo,
      'nota': nota,
    };
  }

  Nota copyWith(
          {int id, int usuarioId, DateTime data, String titulo, String nota}) =>
      Nota(
        id: id ?? this.id,
        usuarioId: usuarioId ?? this.usuarioId,
        data: data ?? this.data,
        titulo: titulo ?? this.titulo,
        nota: nota ?? this.nota,
      );
  @override
  String toString() {
    return (StringBuffer('Nota(')
          ..write('id: $id, ')
          ..write('usuarioId: $usuarioId, ')
          ..write('data: $data, ')
          ..write('titulo: $titulo, ')
          ..write('nota: $nota')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      ((((id.hashCode) * 31 + usuarioId.hashCode) * 31 + data.hashCode) * 31 +
              titulo.hashCode) *
          31 +
      nota.hashCode;
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Nota &&
          other.id == id &&
          other.usuarioId == usuarioId &&
          other.data == data &&
          other.titulo == titulo &&
          other.nota == nota);
}

class $NotasTable extends Notas implements TableInfo<Notas, Nota> {
  final GeneratedDatabase _db;
  $NotasTable(this._db);
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id =>
      _id ??= GeneratedIntColumn('id', false, hasAutoIncrement: true);
  GeneratedIntColumn _usuarioId;
  @override
  GeneratedIntColumn get usuarioId => _usuarioId ??= GeneratedIntColumn(
        'usuario_id',
        false,
      );
  GeneratedDateTimeColumn _data;
  @override
  GeneratedDateTimeColumn get data => _data ??= GeneratedDateTimeColumn(
        'data',
        false,
      );
  GeneratedTextColumn _titulo;
  @override
  GeneratedTextColumn get titulo =>
      _titulo ??= GeneratedTextColumn('titulo', false, maxTextLength: 200);
  GeneratedTextColumn _nota;
  @override
  GeneratedTextColumn get nota =>
      _nota ??= GeneratedTextColumn('nota', false, maxTextLength: 4000);
  @override
  List<GeneratedColumn> get $columns => [id, usuarioId, data, titulo, nota];
  @override
  Notas get asDslTable => this;
  @override
  String get $tableName => 'notas';
  @override
  bool validateIntegrity(Nota instance, bool isInserting) =>
      id.isAcceptableValue(instance.id, isInserting) &&
      usuarioId.isAcceptableValue(instance.usuarioId, isInserting) &&
      data.isAcceptableValue(instance.data, isInserting) &&
      titulo.isAcceptableValue(instance.titulo, isInserting) &&
      nota.isAcceptableValue(instance.nota, isInserting);
  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Nota map(Map<String, dynamic> data) {
    return Nota.fromData(data, _db);
  }

  @override
  Map<String, Variable> entityToSql(Nota d, {bool includeNulls = false}) {
    final map = <String, Variable>{};
    if (d.id != null || includeNulls) {
      map['id'] = Variable<int, IntType>(d.id);
    }
    if (d.usuarioId != null || includeNulls) {
      map['usuario_id'] = Variable<int, IntType>(d.usuarioId);
    }
    if (d.data != null || includeNulls) {
      map['data'] = Variable<DateTime, DateTimeType>(d.data);
    }
    if (d.titulo != null || includeNulls) {
      map['titulo'] = Variable<String, StringType>(d.titulo);
    }
    if (d.nota != null || includeNulls) {
      map['nota'] = Variable<String, StringType>(d.nota);
    }
    return map;
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(const SqlTypeSystem.withDefaults(), e);
  $UsuarioTable _usuario;
  $UsuarioTable get usuario => _usuario ??= $UsuarioTable(this);
  $NotasTable _notas;
  $NotasTable get notas => _notas ??= $NotasTable(this);
  @override
  List<TableInfo> get allTables => [usuario, notas];
}
