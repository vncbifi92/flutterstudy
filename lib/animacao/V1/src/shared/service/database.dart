import 'package:moor_flutter/moor_flutter.dart';
part 'database.g.dart';//flutter packages pub run build_runner build

class Usuario extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(min: 3, max: 100)();
  TextColumn get senha => text().withLength(min: 3, max: 100)();
  TextColumn get email => text().withLength(min: 3, max: 100)();
}

class Notas extends Table{
  IntColumn get id => integer().autoIncrement()();
  IntColumn get usuarioId => integer()();
  DateTimeColumn get data => dateTime()();
  TextColumn get titulo => text().withLength(max: 200)();
  TextColumn get nota => text().withLength(max: 4000)();
}

@UseMoor(tables: [Usuario, Notas])
class MyDatabase extends _$MyDatabase{

  static final MyDatabase instance = MyDatabase._internal();

  MyDatabase._internal() : super(FlutterQueryExecutor.inDatabaseFolder(path: 'db.sqlite'));

  Future<List<UsuarioData>> getUsuario(String nome, String senha){
    print("getUsuario:$nome, $senha");
    return (select(usuario)..where((Usuario u) => u.nome.equals(nome))..where((Usuario u) => u.senha.equals(senha))).get();
  }

  Future<int> addUsuario(UsuarioData u){
    print("addUsuario:${u.toString()}");
    return into(usuario).insert(u);
  }

  Stream<List<Nota>> getNotas(int usuario){
    print("getNota:$usuario");
    Stream<List<Nota>> streamNotas = (select(notas)..where((Notas n) => n.usuarioId.equals(usuario))).watch();
    return streamNotas;
  }

  Future<int> addNota(Nota n){
    print("addNota:${n.toString()}");
    return into(notas).insert(n);
  }

  Future<int> removeNota(Nota n){
    print("removeNota:${n.toString()}");
    return delete(notas).delete(n);
  }

  @override
  int get schemaVersion => 1;
}
