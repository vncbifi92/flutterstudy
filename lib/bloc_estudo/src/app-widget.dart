import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:study/bloc_estudo/src/index/index-bloc.dart';
import 'package:study/bloc_estudo/src/index/index-widget.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocProvider<IndexBloc>(
        bloc: IndexBloc(),
        child: IndexState(),
      ),
    );
  }
}