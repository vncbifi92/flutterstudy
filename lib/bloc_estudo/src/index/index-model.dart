class Pessoa{

  int _id;
  String _nome;
  String _telefone;
  String _email;

  Pessoa(this._nome, this._telefone, this._email);

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  String get telefone => _telefone;

  set telefone(String value) {
    _telefone = value;
  }

  String get nome => _nome;

  set nome(String value) {
    _nome = value;
  }

  @override
  String toString() {
    return 'Pessoa{_id: $_id, _nome: $_nome, _telefone: $_telefone, _email: $_email}';
  }

  @override
  bool operator ==(other) {
    Pessoa p2 = other;
    if(this.email == p2.email && this.id == p2.id && this.telefone == p2.telefone && this.nome == p2.nome)
      return true;
    else
      return false;
  }

  @override
  int get hashCode => super.hashCode;

  Pessoa.fromMap(Map<String, dynamic> map) {
    _id = map['id'];
    _nome = map['nome'];
    _telefone = map['telefone'];
    _email = map['email'];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'nome': this._nome,
      'telefone': this._telefone,
      'email': this._email
    };
    if (_id != null) {
      map["id"] = _id;
    }
    return map;
  }

}