import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:study/bloc_estudo/src/index/index-bloc.dart';
import 'package:study/bloc_estudo/src/index/index-model.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class IndexState extends StatelessWidget {

  final formPessoaKey = GlobalKey<FormState>();

  _salvaPessoa(IndexBloc bloc) {
    final form = formPessoaKey.currentState;
    if (form.validate()) {
      form.save();
      bloc.add();
      bloc.reset();
    }
  }

  _editarPessoa(IndexBloc bloc, Pessoa pessoa) {
    bloc.salvaNome(pessoa.nome);
    bloc.salvaTelefone(pessoa.telefone);
    bloc.salvaEmail(pessoa.email);
    bloc.salvaPessoa(pessoa);
  }

  _removePessoa(IndexBloc bloc, Pessoa pessoa) {
    bloc.salvaNome(pessoa.nome);
    bloc.salvaTelefone(pessoa.telefone);
    bloc.salvaEmail(pessoa.email);
    bloc.salvaPessoa(pessoa);
    bloc.remove();
    bloc.reset();
  }

 _ligar(IndexBloc bloc, Pessoa pessoa) {
    bloc.salvaTelefone(pessoa.telefone);
    bloc.ligar();
  }

  _menssagem(IndexBloc bloc, Pessoa pessoa) {
    bloc.salvaTelefone(pessoa.telefone);
    bloc.menssagem();
  }

  _email(IndexBloc bloc, Pessoa pessoa) {
    bloc.salvaEmail(pessoa.email);
    bloc.email();
  }

  _whatsApp(IndexBloc bloc, Pessoa pessoa) {
    bloc.salvaTelefone(pessoa.telefone);
    bloc.whatsApp();
  }

  Widget _listaPessoas(IndexBloc bloc) {
    return StreamBuilder(
      stream: bloc.outList,
      builder: (BuildContext context, AsyncSnapshot<List<Pessoa>> snapshot) {
        if (snapshot.hasData) {
          return Container(
            child: ListView.builder(
              itemBuilder: (context, index) {
                if (index < snapshot.data.length) {
                  return Slidable(
                    child: Column(
                      children: <Widget>[
                        GestureDetector(
                          onLongPress: (){
                            _removePessoa(
                              bloc,
                              snapshot.data.elementAt(index)
                            );
                          },
                          onTap: () {
                            Navigator.pop(context);
                            _editarPessoa(
                              bloc,
                              snapshot.data.elementAt(index)
                            );
                          },
                          child: Card(
                              color: Color.fromRGBO(147, 251, 255, 0.7),
                              elevation: 4,
                              child: ListTile(
                                title: Text(snapshot.data
                                  .elementAt(index)
                                  .nome),
                                subtitle: Text(snapshot.data
                                  .elementAt(index)
                                  .telefone),
                              ),
                            ),
                        ),
                      ],
                    ),
                    delegate: SlidableDrawerDelegate(),
                    actionExtentRatio: 0.25,
                    actions: <Widget>[
                      new IconSlideAction(
                          caption: 'Mensagem',
                          color: Colors.orange,
                          icon: Icons.message,
                          onTap: () => _menssagem(bloc, snapshot.data.elementAt(index))
                        ),
                      new IconSlideAction(
                          caption: 'Ligar',
                          color: Colors.green,
                          icon: Icons.phone,
                          onTap: () => _ligar(bloc, snapshot.data.elementAt(index))
                      ),
                      new IconSlideAction(
                          caption: 'E-mail',
                          color: Colors.blue,
                          icon: Icons.email,
                          onTap: () => _email(bloc, snapshot.data.elementAt(index))
                      ),
                      new IconSlideAction(
                          caption: 'WhatsApp',
                          color: Colors.lightGreen,
                          icon: MdiIcons.whatsapp ,
                          onTap: () => _whatsApp(bloc, snapshot.data.elementAt(index))
                      ),
                    ],
                  );
                }
              }
            ),
          );
        } else {
          return Divider();
        }
      }
    );
  }

  Widget _cadastroPessoa(IndexBloc bloc) {
    return Form(
      key: formPessoaKey,
      child: SafeArea(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8),
              child: Chip(
                avatar: Icon(Icons.person),
                label: Text(
                  "Contato",
                  textScaleFactor: 2.0,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0)
                ),
                color: Color.fromRGBO(37, 247, 255, 0.5),
                elevation: 6,
                child: SafeArea(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: StreamBuilder(
                          stream: bloc.outNome,
                          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                            return TextFormField(
                              validator: (value){
                                if(value.isEmpty){
                                  return "Informe o nome do contato.";
                                }
                              },
                              controller: TextEditingController(
                                text: snapshot.data,
                              ),
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                icon: Icon(Icons.perm_contact_calendar),
                                labelText: "Nome",
                              ),
                              onSaved: (String val) {
                                bloc.salvaNome(val);
                              }
                            );
                          }
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: StreamBuilder(
                          stream: bloc.outTelefone,
                          builder: (BuildContext context,
                              AsyncSnapshot<String> snapshot) {
                            return TextFormField(
                              validator: (value){
                                if(value.isEmpty){
                                  return "Informe o telefone do contato.";
                                }
                              },
                              keyboardType: TextInputType.phone,
                              controller:  MaskedTextController(
                                  mask: "(00)00000-0000",
                                  text: snapshot.data
                              ),
                              decoration: InputDecoration(
                                icon: Icon(Icons.phone),
                                labelText: "Telefone",
                                hintText: "(00)00000-0000",
                              ),
                              onSaved: (String val) {
                                bloc.salvaTelefone(val);
                              }
                            );
                          }
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: StreamBuilder(
                          stream: bloc.outEmail,
                          builder: (BuildContext context,
                              AsyncSnapshot<String> snapshot) {
                            return TextFormField(
                              validator: (value){
                                if(value.isEmpty){
                                  return "Informe o telefone do contato.";
                                }
                              },
                              keyboardType: TextInputType.emailAddress,
                              controller: TextEditingController(
                                  text: snapshot.data
                              ),
                              decoration: InputDecoration(
                                icon: Icon(Icons.email),
                                labelText: "Email",
                              ),
                              onSaved: (String val) {
                                bloc.salvaEmail(val);
                              }
                            );
                          }
                        ),
                      ),
                      Divider(
                        color: Colors.transparent,
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //Bloc responsavel pela gerencia da lista de pessoas
    final IndexBloc bloc = BlocProvider.of<IndexBloc>(context);
    bloc.carregaLista();
    return Scaffold(
      appBar: AppBar(
        title: Text("Contact Me"),
      ),
      drawer: SafeArea(
        child: Drawer(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8),
                child: Chip(
                  avatar: Icon(Icons.people),
                  label: Text(
                    "Contatos",
                    textScaleFactor: 2.0,
                  ),
                ),
              ),
              Expanded(child: _listaPessoas(bloc))
            ],
          )
        ),
      ),
      body: ListView(
        children: <Widget>[_cadastroPessoa(bloc)]
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          _salvaPessoa(bloc);
        },
      )
    );
  }
}