import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;

import 'package:study/bloc_estudo/src/index/index-model.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if(_db != null){
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "main.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE Pessoa(  id INTEGER PRIMARY KEY AUTOINCREMENT"
            ", nome TEXT"
            ", telefone TEXT"
            ", email TEXT)");
  }

  Future closeDb() async {
    var dbClient = await db;
    dbClient.close();
  }

  Future<Pessoa> insertPessoa(Pessoa pessoa) async {
    var dbClient = await db;
    pessoa.id = await dbClient.insert("Pessoa", pessoa.toMap());
    return pessoa;
  }

  Future<int> deletPessoa(int id) async {
    var dbClient = await db;
    return await dbClient.delete("Pessoa", where: "id = ?", whereArgs: [id]);
  }

  Future<int> updatePessoa(Pessoa pessoa) async {
    var dbClient = await db;
    return await dbClient.update("Pessoa", pessoa.toMap(),
        where: "id = ?", whereArgs: [pessoa.id]);
  }

  Future<List<Pessoa>> getPessoas() async {
    List<Pessoa> pessoas = new List<Pessoa>();
    var dbClient = await db;
    List<Map> maps = await dbClient.query("Pessoa");
    if (maps.length > 0) {
      for (Map pessoa in maps) {
        pessoas.add(Pessoa.fromMap(pessoa));
      }
    }
    return  pessoas;
  }

}