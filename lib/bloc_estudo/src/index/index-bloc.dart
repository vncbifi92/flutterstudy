import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:study/bloc_estudo/src/index/index-model.dart';
import 'package:study/bloc_estudo/src/index/index-service.dart';
import 'package:url_launcher/url_launcher.dart';

class IndexBloc extends BlocBase{

  DatabaseHelper databaseHelper = DatabaseHelper.internal();

  var _nome = BehaviorSubject<String>.seeded("");

  Stream<String> get outNome => _nome.stream;

  Sink<String> get inNome => _nome.sink;

  void salvaNome(String nome){
    _nome.value = nome;
    inNome.add(_nome.value);
    print("nome");
  }

  var _telefone = BehaviorSubject<String>.seeded("");

  Stream<String> get outTelefone => _telefone.stream;

  Sink<String> get inTelefone => _telefone.sink;

  void salvaTelefone(String telefone){
    _telefone.value = telefone;
    inTelefone.add(_telefone.value);
    print("telefone");
  }

  var _email = BehaviorSubject<String>.seeded("");

  Stream<String> get outEmail => _email.stream;

  Sink<String> get inEmail => _email.sink;

  void salvaEmail(String email){
    _email.value = email;
    inEmail.add(_email.value);
    print("email");
  }

  var _pessoa = BehaviorSubject<Pessoa>.seeded(Pessoa("", "", ""));

  Stream<Pessoa> get outPessoa => _pessoa.stream;

  Sink<Pessoa> get inPessoa => _pessoa.sink;

  void salvaPessoa(Pessoa pessoa){
    _pessoa.value = pessoa;
    inPessoa.add(_pessoa.value);
    print("pessoa");
  }

  var _listPessoa = BehaviorSubject<List<Pessoa>>.seeded(List<Pessoa>());

  Stream<List<Pessoa>> get outList => _listPessoa.stream;

  Sink<List<Pessoa>> get inList => _listPessoa.sink;

  void add(){
    Pessoa value = Pessoa(_nome.value, _telefone.value, _email.value);
    databaseHelper.insertPessoa(value);
    _listPessoa.value.add(value);
    _listPessoa.value.sort((a, b) {
      return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
    });
    inList.add(_listPessoa.value);
    print("salva lista");
  }

  void remove(){
    databaseHelper.deletPessoa(_pessoa.value.id);
    _listPessoa.value.remove(_pessoa.value);
    inList.add(_listPessoa.value);
    print("remove lista");
  }

  void reset(){
    salvaNome("");
    salvaTelefone("");
    salvaEmail("");
    salvaPessoa(Pessoa("", "", ""));
  }

  void carregaLista() async{
    List<Pessoa> pessoas = await databaseHelper.getPessoas();
    pessoas.sort((a, b) {
      return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
    });
    _listPessoa.value = pessoas;
  }

  void ligar() async{
    if(await canLaunch("tel:+55" + _telefone.value)) {
      await launch("tel:+55" + _telefone.value);
    }
    reset();
    print("ligar");
  }

  void menssagem() async{
    if(await canLaunch("sms:+55" + _telefone.value.replaceAll("-", "").replaceAll("(", "").replaceAll(")", ""))) {
      await launch("sms:+55" + _telefone.value.replaceAll("-", "").replaceAll("(", "").replaceAll(")", ""));
    }
    reset();
    print("menssagem");
  }

  void email() async{
    if(await canLaunch("mailto:"+_email.value)){
      await launch("mailto:"+_email.value);
    }
    reset();
    print("e-mail");
  }

  void whatsApp() async{
    var whatsappUrl ="whatsapp://send?phone=+55"+_telefone.value.replaceAll("-", "").replaceAll("(", "").replaceAll(")", "");
    if(await canLaunch(whatsappUrl)) {
      launch(whatsappUrl);
    }
    reset();
    print("whatsApp");
  }

  @override
  void dispose() {
    _listPessoa.close();
    _nome.close();
    _telefone.close();
    _email.close();
    _pessoa.close();
  }

}